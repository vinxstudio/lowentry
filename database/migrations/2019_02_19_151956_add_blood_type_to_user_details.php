<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBloodTypeToUserDetails extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_details', function (Blueprint $table) {
			//
			$table->string('blood_type')->after('weight')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_details', function (Blueprint $table) {
			//
			$table->dropColumn('blood_type');
		});
	}

}
