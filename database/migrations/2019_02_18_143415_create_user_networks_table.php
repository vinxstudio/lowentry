<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserNetworksTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_networks', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id_owner')->unique()->nullable()->foreign()->references('id')->on('users')->onDelete('set null')->comment('The owner of this row of the downline of the user_id_sponsor');
			$table->integer('user_id_sponsor')->nullable()->foreign()->references('id')->on('users')->onDelete('set null')->comment('The sponsor of the owner of the user_id');
			$table->enum('status', ['Approved', 'Declined', 'Pending'])->default('Pending')->nullable(true)->comment('Approved, Declined, or still Pending');
			$table->dateTime('date_status')->default('0000-00-00 00:00:00')->comment('The date when this status is approved or declined');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_networks');
	}

}
