<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAddressesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_addresses', function (Blueprint $table) {
			$table->increments('id');

			$table->enum('address_type', ['Provincial', 'Present', 'Temporary', 'Overseas'])->default('Present')->comment('Provincial, Present, Temporary, Overseas');

			$table->string('from_table_name')->comment('users, user_people_relations, etc');
			$table->integer('from_table_id')->comment('the id of that table');

			$table->string('street');
			$table->string('barangay');
			$table->string('town');
			$table->string('city');
			$table->dateTime('since');
			$table->integer('zip_code');




			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_addresses');
	}

}
