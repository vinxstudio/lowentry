<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ChangeEnumValuesInUserNetworks extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		DB::statement("ALTER TABLE user_networks MODIFY status ENUM('Pending', 'Sponsor Disapproved', 'Admin Disapproved', 'Sponsor Approved', 'Admin Approved')");

		#Schema::table('user_networks', function(Blueprint $table)
		#{
			//
		#	$table->drop
		#});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		#Schema::table('user_networks', function (Blueprint $table) {
		#	//
		#});
		DB::statement("ALTER TABLE user_networks MODIFY status ENUM('Pending', 'Sponsor Disapproved', 'Admin Disapproved', 'Sponsor Approved', 'Admin Approved')");

	}

}
