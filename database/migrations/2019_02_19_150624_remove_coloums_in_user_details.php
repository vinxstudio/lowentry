<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveColoumsInUserDetails extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_details', function (Blueprint $table) {
			//
			$table->dropColumn('coop_id');
			$table->dropColumn('bank_name');
			$table->dropColumn('account_name');
			$table->dropColumn('account_number');
			$table->dropColumn('truemoney');
			$table->dropColumn('title');
			$table->dropColumn('suffix');
			$table->dropColumn('no_dependents');
			$table->dropColumn('present_street');
			$table->dropColumn('present_address_details');
			$table->dropColumn('present_barangay');
			$table->dropColumn('present_town');
			$table->dropColumn('present_city');
			$table->dropColumn('present_province');
			$table->dropColumn('present_region');
			$table->dropColumn('present_since');
			$table->dropColumn('present_zipcode');
			$table->dropColumn('provincial_city');
			$table->dropColumn('provincial_address_details');
			$table->dropColumn('provincial_barangay');
			$table->dropColumn('provincial_town');
			$table->dropColumn('provincial_province');
			$table->dropColumn('provincial_region');
			$table->dropColumn('provincial_since');
			$table->dropColumn('provincial_zipcode');
			$table->dropColumn('employer_name');
			$table->dropColumn('job_title');
			$table->dropColumn('date_hired');
			$table->dropColumn('job_status');

			$table->dropColumn('educational_attainment');
			$table->dropColumn('school_last_attended');
			$table->dropColumn('education_year');
			$table->dropColumn('cellphone_no');
			$table->dropColumn('other_contact_no');
			$table->dropColumn('home_tel_no');
			$table->dropColumn('spouse_tel_no');
			$table->dropColumn('province_tel_no');
			$table->dropColumn('tin');
			$table->dropColumn('GSIS');
			$table->dropColumn('SSS');
			$table->dropColumn('philhealth');
			$table->dropColumn('pagibig');
			$table->dropColumn('s_last_name');
			$table->dropColumn('s_first_name');
			$table->dropColumn('s_middle_name');
			$table->dropColumn('s_suffix');
			$table->dropColumn('s_gender');
			$table->dropColumn('s_birth_date');
			$table->dropColumn('s_occupation');
			$table->dropColumn('f_last_name');
			$table->dropColumn('f_first_name');
			$table->dropColumn('f_middle_name');
			$table->dropColumn('f_suffix');
			$table->dropColumn('m_last_name');
			$table->dropColumn('m_first_name');
			$table->dropColumn('m_middle_name');
			$table->dropColumn('m_suffix');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_details', function (Blueprint $table) {
			//
		});
	}

}
