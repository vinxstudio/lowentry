<header id="header">
    <div class="header-right">
        <div class="navbar navbar-toolbar">
            <ul class="nav navbar-nav navbar-left">
                <li id="tour-2" class="navbar-minimize">
                    <a href="javascript:void(0);" title="Minimize sidebar">
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                @if($theUser->role == 'member' && !empty($myaccounts))
                <li id="tour-6" class="header-dropdown-username">
                    {{ Form::open([
                            'class'=>'form-horizontal form-bordered',
                            'url' => 'member/dashboard/login', 
                            'method' => 'POST'
                            ]) }}
                    <label>USERNAME: </label>
                    <select name="goto_user_id" style="padding: 5px;">
                        <option value="0"> - Select Username - </option>
                        @foreach( $myaccounts as $myaccount )
                            @if($myaccount->user->role == 'member')
                                <option value="{{ $myaccount->user->id }}">{{ $myaccount->user->username }}</option>
                            @endif
                        @endforeach
                    </select>
                    <input type="submit" value="GO">
                    {{ Form::close() }}
                </li>
                @endif
                <!-- Start profile -->
                <li id="tour-6" class="dropdown navbar-profile">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="meta">
                            
                                    <span class="avatar"><img src="/public/member_photo/avatar.jpeg" class="img-circle" alt="admin" width="35" height="35"></span>
                                    <span class="text hidden-xs hidden-sm text-muted">{{ $theUser->details->first_name }}</span>
                                    <span class="caret"></span>
                                </span>
                    </a>
                    <!-- Start dropdown menu -->
                    <ul class="dropdown-menu animated flipInX">
                        <li class="dropdown-header">{{ Lang::get('labels.account') }}</li>
                        <li><a href="{{ url(Request::segment(1).'/profile') }}"><i class="fa fa-user"></i>{{ Lang::get('labels.view_profile') }}</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ url('logout') }}"><i class="fa fa-sign-out"></i>{{ Lang::get('labels.logout') }}</a></li>
                    </ul>
                    <!--/ End dropdown menu -->
                </li><!-- /.dropdown navbar-profile -->
               

            </ul>
           
        </div>
    </div>

</header> 