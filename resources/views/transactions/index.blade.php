@extends('layouts.members')
@section('content')
<div class="main">
	@if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
<div class="tab">
		<button class="tablinks" onclick="openCity(event, 'Driver')">Driver</button>
		<button class="tablinks" onclick="openCity(event, 'Passenger')">Passenger</button>
	  </div>
	  
	  <!-- Tab content -->
	  <div id="Driver" class="tabcontent">
			<table class="table table-bordered table-hover">
					<thead>
						<th>Particular</th>
						<th>User I.D</th>
						<th>Level</th>
						<th>Amount Paid</th>
						<th>Percetages</th>
						<th>Override Income</th>
						<th>Reffered by:</th>
						<th>Reffered Name:</th>
						<th>Category</th>
						<th>Status</th>
						<th>Driver In Charge</th>
						<th>Date </th>
						<th>Time</th>
					</thead>  
			</table>
	  </div>
	  
	  <div id="Passenger" class="tabcontent">
			<table class="table table-bordered table-hover">
					<thead>
						<th>Particular</th>
						<th>User I.D</th>
						<th>Level</th>
						<th>Amount Paid</th>
						<th>Percetages</th>
						<th>Override Income</th>
						<th>Reffered by:</th>
						<th>Reffered Name:</th>
						<th>Category</th>
						<th>Status</th>
						<th>Driver In Charge</th>
						<th>Date </th>
						<th>Time</th>
					</thead> 
	  </div>
	    <tbody>
	    	@foreach( $transactions as $transaction)
	            <tr>
	            	<td>{{ $transaction->id }}</td>
	            	<td><!--{{ $transaction->requester }}--></td>
	            	<td>{{ str_replace('App\\Models\\', '', $transaction->details_type) }}</td>
	                <td>{{ $transaction->status_string }}</td>
	                <td>{{ isset($transaction->cost)? number_format($transaction->cost, 2) : ""}}</td>
	                <td>{{ isset($transaction->amount)? number_format($transaction->amount, 2) : ""}}</td>
	                <td>{{ $transaction->created_at }}</td>
	                <td><!--<a href="{{ URL::to('member/transactions/'.$transaction->id) }}">-->View</td>
	            </tr>
	        @endforeach
	        <tr class="success">
	        </tr>
	    </tbody>
	</table>
	<div>{{ $transactions->render() }}</div>
</div>
@stop
<style>
	/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons that are used to open the tab content */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: white;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
</style>
<script>
	function openCity(evt, cityName) {
  // Declare all variables
  var i, tabcontent, tablinks;

  // Get all elements with class="tabcontent" and hide them
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the button that opened the tab
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
	</script>