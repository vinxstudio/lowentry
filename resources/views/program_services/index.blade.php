@extends('layouts.members')
@section('content')
<div class="content row text-center w-75 mx-auto">
    <div class="main">
        <span class="h3">PRODUCTS AND SERVICES</span>
        <div class="row py-4">
            <div class="col my-4">
                {{-- <a class="text-dark" href="{{ URL::to('member/programServices/savings') }}"> --}}
                    <span class="orange bg rounded-circle mb-3 p-5 fa-layers fa-4x fa-inverse">
                        <i class="fas fa-taxi"></i>
                    </span>
                    <br>SUNDO-HATID OFW TAXI SERVICES
                </a>
            </div>
            <div class="col my-4">
                {{-- <a class="text-dark" href="{{ URL::to('member/programServices/financing') }}"> --}}
                    <span class="orange bg rounded-circle mb-3 p-5 fa-layers fa-4x fa-inverse">
                            <i class="fas fa-cogs"></i>
                    </span>
                    <br>SOLAR AND WINDMILLS
                </a>
            </div>
            <div class="col my-4">
                {{-- <a class="text-dark" href="{{ URL::to('member/programServices/hospitalization') }}"> --}}
                    <span class="orange bg rounded-circle mb-3 p-5 fa-layers fa-4x fa-inverse">
                            <i class="fas fa-tint" data-fa-transform="shrink-2"></i>
                    </span>
                    <br>WATER REFILLING SYSTEM
                </a>
            </div>
            <div class="col my-4">
                {{-- <a class="text-dark" href="{{ URL::to('member/programServices/education') }}"> --}}
                    <span class="orange bg rounded-circle mb-3 p-5 fa-layers fa-4x fa-inverse">
                        <i class="fas fa-university" data-fa-transform="shrink-2"></i>
                    </span>
                    <br>OFW CORPORATE BANK
                </div>
        </div>
        <div class="row py-4">
            <div class="col my-4">
                {{-- <a class="text-dark" href="{{ URL::to('member/programServices/education') }}"> --}}
                    <span class="orange bg rounded-circle mb-3 p-5 fa-layers fa-4x fa-inverse">
                        <i class="fas fa-money-bill-alt" data-fa-transform="shrink-2"></i>
                    </span>
                    <br>LOAN AND SAVINGS
                </div>
           
            <div class="col my-4">
                {{-- <a class="text-dark" href="{{ URL::to('member/programServices/education') }}"> --}}
                    <span class="orange bg rounded-circle mb-3 p-5 fa-layers fa-4x fa-inverse">
                        <i class="far fa-hospital" data-fa-transform="shrink-2"></i>
                    </span>
                    <br>MEDICAL SERVICES
            </div>
            <div class="col my-4">
                {{-- <a class="text-dark" href="{{ URL::to('member/programServices/education') }}"> --}}
                    <span class="orange bg rounded-circle mb-3 p-5 fa-layers fa-4x fa-inverse">
                        <i class="fa fa-users" data-fa-transform="shrink-2"></i>
                    </span>
                    <br>MANPOWER SERVICES
            </div>
            <div class="col my-4">
                {{-- <a class="text-dark" href="{{ URL::to('member/programServices/education') }}"> --}}
                    <span class="orange bg rounded-circle mb-3 p-5 fa-layers fa-4x fa-inverse">
                        <i class="fas fa-money-bill-alt" data-fa-transform="shrink-2"></i>
                    </span>
                    <br>INSURANCE SERVICES
            </div>
        </div>
@stop