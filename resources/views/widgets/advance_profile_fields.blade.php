
<style>
    .display-block{
        display: block;
    }

</style>
<div class="clearfix"></div>


<div class="red-color">
    <p>
        <i>
            @if(session()->has('need_profile_update') == true)
                {{ session('need_profile_update_message') }} 
            @elseif(session()->has('member_pending') == true)
                {{ session('member_pending_message') }}
            @endif
        </i>
    </p>
</div>

<div class="red-color">
    <p>
        @foreach ($errors->all() as $error)
            <i class="display-block">* {{ $error }}</i>
        @endforeach
    </p>
</div>

@if(session()->has('status') && session('status') == 'success')
    <div class="alert alert-success">{{ session('message') }}</div>
@endif

<div class="col-md-12"><legend style=color:#019541;>USER INFO</legend></div>
<div class="clearfix"></div>
<div class="row">
    {{-- Date of Application --}}
    <div class="col-md-3 col-sm-12 form-group">
        <label class="control-label">COOP ID</label>
        <input type="text" placeholder="COOP ID" class="form-control" value={{ old('membership_id', isset($user->membership_id) ? $user->membership_id : null ) }} class="form-control" readonly>
    </div>
    @if(isset($user->date_of_application) && $user->date_of_application !== '0000-00-00 00:00:00')
        <div class="col-md-3 col-sm-12 form-group">
            <label class="control-label">Date of Application</label>
            <input type="text" placeholder="Date of application" class="form-control" value="{{ date('F d, Y', strtotime($user->date_of_application)) }}" />
        </div>
    @else
        <div class="col-md-3 col-sm-12 form-group">
            <label class="control-label">Date of Application</label>
            <input type="date" name="date_of_application" placeholder="Date of application" class="form-control red-background" />
        </div>
    @endif

    {{-- Referred By ID --}}
    <div class="col-md-3 col-sm-12 form-group">
        <label class="control-label">Referred By: (ID)</label>
        <input type="number" disabled name="ref_id" placeholder="ID" value="{{ old('ref_id', isset($user->details->ref_id) ? $user->details->ref_id : null ) }}" class="form-control" >
    </div>

    {{-- Reffered By Name --}}
    <div class="col-md-3 col-sm-12 form-group">
        <label class="control-label">Referred By: (Name)</label>
        <input type="text" disabled name="ref_name" placeholder="Name" value="{{ old('ref_name', isset($user->details->ref_name) ? $user->details->ref_name : null ) }}" class="form-control" >
    </div>

</div>

<div class="row">
    {{-- Password --}}
    <div class="col-md-3 col-sm-12 form-group">
        <label class="control-label">Passport#</label>
        <input type="text" disabled name="passport" placeholder="Passport" value="{{ old('passport', isset($user->details->passport) ? $user->details->passport : null ) }}" class="form-control" >
    </div>
    {{-- Issued At --}}
    <div class="col-md-3 col-sm-12 form-group">
        <label class="control-label">Issued At</label>
        <input type="date" disabled name="issued_at" placeholder="Issued At" value="{{ old('issued_at', isset($user->details->issued_at) ? $user->details->issued_at : null ) }}" class="form-control" >
    </div>
</div>

{{-- First Name --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.first_name') }}</label>
    <input type="text" name="first_name" placeholder="{{ Lang::get('members.first_name') }}" value="{{ old('first_name', isset($user->details->first_name) ? $user->details->first_name : null ) }}" class="form-control" readonly>
</div>

{{-- Last Name --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.last_name') }}</label>
    <input type="text" name="last_name" placeholder="{{ Lang::get('members.last_name') }}" value="{{ old('last_name', isset($user->details->last_name) ? $user->details->last_name : null) }}" class="form-control" readonly>
</div>

{{-- Middle Name --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.middle_name') }}</label>
    <input type="text" name="middle_name" placeholder="{{ Lang::get('members.middle_name') }}" value="{{ old('middle_name', isset($user->details->middle_name) ? $user->details->middle_name : null) }}" class="form-control" readonly>
</div>
<div class="col-md-3 col-sm-12 form-group">
        <label class="control-label">User Type</label>
        <input type="text" name="user_type" placeholder="User Type" value="{{ old('middle_name', isset($user->details->user_type) ? $user->details->user_type : null) }}" class="form-control" readonly>
    </div>
{{-- <div class="col-md-3 col-sm-12 form-group">
        <label class="control-label">User Type</label>
        <div class="custom-select" style="width:155px;" name="user">
               {{ Form::select('user_type',  [
                'label' => 'User Type',
                'Passenger' => 'Passenger',
                'Driver' => 'Driver',
                'Both' => 'Both'   
             ])}}
           </div> --}}

<div class="clearfix"></div>

{{-- Birth Date --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.birth_date') }}</label>
    <input type="text" name="birth_date" placeholder="0000-00-00 / Y-M-D" value="{{ old('birth_date', isset($user->details->birth_date) ? $user->details->birth_date : '') }}" class="form-control">
</div>

{{-- Birth Place --}}
<div class="col-md-6 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.birth_place') }}</label>
    <input type="text" name="birth_place" placeholder="{{ Lang::get('members.birth_place') }}" value="{{ old('birth_place', isset($user->details->birth_place) ? $user->details->birth_place : null) }}" class="form-control">
</div>

{{-- Profession --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.profession') }}</label>
    <input type="text" name="profession" placeholder="{{ Lang::get('members.profession') }}" value="{{ old('profession', isset($user->details->profession) ? $user->details->profession : null) }}" class="form-control">
</div>

<div class="clearfix"></div>

{{-- Gender --}}
<div class="col-md-2 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.gender') }}</label>
    <input type="text" name="gender" placeholder="{{ Lang::get('members.gender') }}" 
        value="{{ old('gender', isset($user->details->gender) ? $user->details->gender : null) }}" 
        class="form-control @if(!isset($user->details->gender) || isset($user->details->gender) && $user->details->gender == '') red-background @endif">
</div>

{{-- Religion --}}
<div class="col-md-2 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.religion') }}</label>
    <input type="text" name="religion" placeholder="{{ Lang::get('members.religion') }}" 
        value="{{ old('religion', isset($user->details->religion) ? $user->details->religion : null) }}" 
        class="form-control @if(!isset($user->details->religion) || isset($user->details->religion) && $user->details->religion == '') red-background @endif">
</div>

{{-- Nationality --}}
<div class="col-md-2 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.nationality') }}</label>
    <input type="text" name="nationality" placeholder="{{ Lang::get('members.nationality') }}" 
        value="{{ old('nationality', isset($user->details->nationality) ? $user->details->nationality : null) }}" 
        class="form-control @if(!isset($user->details->nationality) || isset($user->details->nationality) && $user->details->nationality == '') red-background @endif ">
</div>

{{-- Civil Status --}}
<div class="col-md-2 col-sm-12 form-group">
    <label class="control-label">Civil Status</label>
    <input type="text" name="civil_status" placeholder="Civil Status" 
        value="{{ old('status', isset($user->details->civil_status) ? $user->details->civil_status : null) }}" 
        class="form-control @if(!isset($user->details->civil_status) || isset($user->details->civil_status) && $user->details->civil_status == '') red-background @endif ">
</div>

{{-- Height --}}
<div class="col-md-2 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.height') }}</label>
    <input type="text" name="height" placeholder="{{ Lang::get('members.height') }}" value="{{ old('height', isset($user->details->height) ? $user->details->height : 0) }}" class="form-control">
</div>

{{-- Weight --}}
<div class="col-md-2 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.weight') }}</label>
    <input type="text" name="weight" placeholder="{{ Lang::get('members.weight') }}" value="{{ old('weight', isset($user->details->weight) ? $user->details->weight : 0) }}" class="form-control">
</div>

{{-- Blood Type --}}
<div class="col-md-2 col-sm-12 form-group">
    <label class="control-label">Blood Type</label>
    <input type="text" name="blood_type" placeholder="Blood Type" 
        value="{{ old('blood_type', isset($user->details->blood_type) ? $user->details->blood_type : null) }}" 
        class="form-control @if(!isset($user->details->blood_type) || isset($user->details->blood_type) && $user->details->blood_type == '') red-background @endif ">
</div>

<div class="clearfix"></div>
<div class="col-md-12"><legend style=color:#019541;>ADDRESS</legend></div>
<div class="clearfix"></div>

<!-- present address start -->
<h4>Present Address (Mailing Address)</h4>
<div class="clearfix"></div>

{{-- Present Street --}}
<div class="col-md-4 col-sm-12 form-group">
    <label class="control-label">Street</label>
    <input type="text" name="present_street" placeholder="Street" 
        value="{{ old('present_street', isset($user->address()->street) ? $user->address()->street : null) }}" 
        class="form-control @if(!isset($user->address()->street) || isset($user->address()->street) && $user->address()->street == '') red-background @endif">
</div>

{{-- Present Baranggay --}}
<div class="col-md-4 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.barangay') }}</label>
    <input type="text" name="present_barangay" placeholder="{{ Lang::get('members.barangay') }}" 
        value="{{ old('present_barangay', isset($user->address()->barangay) ? $user->address()->barangay : null) }}" 
        class="form-control @if(!isset($user->address()->barangay) || isset($user->address()->barangay) && $user->address()->barangay == '') red-background @endif">
</div>

{{-- Present Town --}}
<div class="col-md-4 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.town') }}</label>
    <input type="text" name="present_town" placeholder="{{ Lang::get('members.town') }}" 
        value="{{ old('present_town', isset($user->address()->town) ? $user->address()->town : null) }}"
        class="form-control @if(!isset($user->address()->town) || isset($user->address()->town) && $user->address()->town == '') red-background @endif">
</div>


{{-- Present City --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.city') }}</label>
    <input type="text" name="present_city" placeholder="{{ Lang::get('members.city') }}" 
        value="{{ old('present_city', isset($user->address()->city) ? $user->address()->city : null) }}" 
        class="form-control @if(!isset($user->address()->city) || isset($user->address()->city) && $user->address()->city == '') red-background @endif">
</div>

{{-- Present Since --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">Since</label>
    <input type="text" name="present_since" placeholder="0000-00-00 / Y-M-D" 
        value="{{ isset($user->address()->since) && $user->address()->since != '0000-00-00 00:00:00' ? date('F d, Y', strtotime($user->address()->since)) : null }}" 
        class="form-control">
</div>

{{-- Present ZipCode --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.zipcode') }}</label>
    <input type="text" name="present_zipcode" placeholder="{{ Lang::get('members.zipcode') }}" 
        value="{{ old('present_zipcode', isset($user->address()->zip_code) ? $user->address()->zip_code : null) }}"
        class="form-control @if(!isset($user->address()->zip_code) || isset($user->address()->zip_code) && $user->address()->zip_code == '') red-background @endif">
</div>
<!-- present address end -->

<div class="clearfix"></div>

<!-- provincial address start -->
<h4>Provincial Address</h4>
<div class="clearfix"></div>

{{-- Provincial Street --}}
<div class="col-md-4 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.street') }}</label>
    <input type="text" disabled name="provincial_street" placeholder="{{ Lang::get('members.street') }}" value="{{ old('provincial_street', isset($user->details->provincial_street) ? $user->details->provincial_street : null) }}" class="form-control">
</div>

{{-- Provincial Barangay --}}
<div class="col-md-4 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.barangay') }}</label>
    <input type="text" disabled name="provincial_barangay" placeholder="{{ Lang::get('members.barangay') }}" value="{{ old('provincial_barangay', isset($user->details->provincial_barangay) ? $user->details->provincial_barangay : null) }}" class="form-control">
</div>

{{-- Provincial Town --}}
<div class="col-md-4 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.town') }}</label>
    <input type="text" disabled name="provincial_town" placeholder="{{ Lang::get('members.town') }}" value="{{ old('provincial_town', isset($user->details->provincial_town) ? $user->details->provincial_town : null) }}" class="form-control">
</div>

<div class="clearfix"></div>

{{-- Provincial City --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.city') }}</label>
    <input type="text" disabled name="provincial_city" placeholder="{{ Lang::get('members.city') }}" value="{{ old('provincial_city', isset($user->details->provincial_city) ? $user->details->provincial_city : null) }}" class="form-control">
</div>

{{-- Provincial Since --}}
<div class="col-md-3 col-sm-12 form-group">
    {{-- {{ validationError($errors, 'provincial_since') }} --}}
    <label class="control-label">{{ Lang::get('members.since') }}</label>
    <input type="text" disabled name="provincial_since" placeholder="{{ Lang::get('members.since') }}" value="{{ old('provincial_since', isset($user->details->provincial_since) ? $user->details->provincial_since : null) }}" class="form-control">
</div>

{{-- Provincial ZipCode --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.zipcode') }}</label>
    <input type="text" disabled name="provincial_zipcode" placeholder="{{ Lang::get('members.zipcode') }}" value="{{ old('provincial_zipcode', isset($user->details->provincial_zipcode) ? $user->details->provincial_zipcode : null) }}" class="form-control">
</div>

<div class="clearfix"></div>

<!-- present address end -->
<h4>Overseas Address</h4>
<div class="clearfix"></div>

{{-- Overseas Street --}}
<div class="col-md-4 col-sm-12 form-group">
    <label class="control-label">Street</label>
    <input type="text" disabled name="overseas_street" placeholder="Street" value="{{ old('overseas_street', isset($user->details->overseas_street) ? $user->details->overseas_street : null) }}" class="form-control">
</div>

{{-- Overseas Baranggay --}}
<div class="col-md-4 col-sm-12 form-group">
    <label class="control-label">Barangay</label>
    <input type="text" disabled name="overseas_barangay" placeholder="Barangay" value="{{ old('overseas_barangay', isset($user->details->overseas_barangay) ? $user->details->overseas_barangay : null) }}" class="form-control">
</div>

{{-- Overseas Town --}}
<div class="col-md-4 col-sm-12 form-group">
    <label class="control-label">Town</label>
    <input type="text" disabled name="overseas_town" placeholder="Town" value="{{ old('overseas_town', isset($user->details->overseas_town) ? $user->details->overseas_town : null) }}" class="form-control">
</div>

<div class="clearfix"></div>

{{-- Overseas City --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">City</label>
    <input type="text" disabled name="overseas_city" placeholder="City" value="{{ old('overseas_city', isset($user->details->overseas_city) ? $user->details->overseas_city : null) }}" class="form-control">
</div>

{{-- Overseas Since --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.since') }}</label>
    <input type="text" disabled name="provincial_since" placeholder="Since" value="{{ old('overseas_since', isset($user->details->overseas_since) ? $user->details->overseas_since : null) }}" class="form-control">
</div>

{{-- Overseas ZipCode --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">Zipcode</label>
    <input type="text" disabled name="overseas_zipcode" placeholder="Zipcode" value="{{ old('overseas_zipcode', isset($user->details->overseas_zipcode) ? $user->details->overseas_zipcode : null) }}" class="form-control">
</div>

<!-- Start Employment -->
<div class="clearfix"></div>
<div class="col-md-12"><legend style=color:#019541;>EMPLOYMENT</legend></div>
<div class="clearfix"></div>

{{-- Employment Name --}}
<div class="col-md-4 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.employer_name') }}</label>
    <input type="text" disabled name="employer_name" placeholder="{{ Lang::get('members.employer_name') }}" value="{{ old('proviemployer_namecial_zipcode', isset($user->details->employer_name) ? $user->details->employer_name : null) }}" class="form-control">
</div>

{{-- Employment Job Title --}}
<div class="col-md-4 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.job_title') }}</label>
    <input type="text" disabled name="job_title" placeholder="{{ Lang::get('members.job_title') }}" value="{{ old('job_title', isset($user->details->job_title) ? $user->details->job_title : null) }}" class="form-control">
</div>

{{-- Employment Date Hired --}}
<div class="col-md-2 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.date_hired') }}</label>
    <input type="text" disabled name="date_hired" placeholder="{{ Lang::get('members.date_hired') }}" value="{{ old('date_hired', isset($user->details->date_hired) ? $user->details->date_hired : '0000-00-00') }}" class="form-control">
</div>

{{-- Employment Status --}}
<div class="col-md-2 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.job_status') }}</label>
    <input type="text" disabled name="job_status" placeholder="{{ Lang::get('members.job_status') }}" value="{{ old('job_status', isset($user->details->job_status) ? $user->details->job_status : null) }}" class="form-control">
</div>

<!-- End Employment -->
<!-- Start Education -->
<div class="clearfix"></div>
<div class="col-md-12"><legend style=color:#019541;>EDUCATION</legend></div>
<div class="clearfix"></div>

{{-- Educational Attainment --}}
<div class="col-md-5 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.educational_attainment') }}</label>
    <input type="text" disabled name="educational_attainment" placeholder="{{ Lang::get('members.educational_attainment') }}" value="{{ old('educational_attainment', isset($user->details->educational_attainment) ? $user->details->educational_attainment : null) }}" class="form-control">
</div>

{{-- Education Last School Attended --}}
<div class="col-md-5 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.school_last_attended') }}</label>
    <input type="text" disabled name="school_last_attended" placeholder="{{ Lang::get('members.school_last_attended') }}" value="{{ old('school_last_attended', isset($user->details->school_last_attended) ? $user->details->school_last_attended : null) }}" class="form-control">
</div>

{{-- Education Year --}}
<div class="col-md-2 col-sm-12 form-group">
    <label class="control-label">{{ Lang::get('members.year') }}</label>
    <input type="text" disabled name="education_year" placeholder="{{ Lang::get('members.year') }}" value="{{ old('education_year', isset($user->details->education_year) ? $user->details->education_year : null) }}" class="form-control">
</div>

<!-- End Education -->

<!-- Start Contact Number -->

<div class="clearfix"></div>
<div class="col-md-12"><legend style=color:#019541;>CONTACT NUMBER</legend></div>
<label>(No dash (-) or slash(/))</label>
<div class="clearfix"></div>

{{-- Contact Number Cellphone Number --}}
<div class="col-md-4 col-sm-12 form-group">
    <label class="control-label">Cellphone Number</label>
    <input type="text" disabled name="cellphone_no" placeholder="Cellphone Number" value="{{ old('cellphone_no', isset($user->details->cellphone_no) ? $user->details->cellphone_no : null) }}" class="form-control">
</div>

{{-- Contact Number Email --}}
<div class="col-md-4 col-sm-12 form-group">
    <label class="control-label">Email</label>
    <input type="text" disabled name="email" placeholder="Email" value="{{ old('email', isset($user->email) ? $user->email : null) }}" class="form-control">
</div>

{{-- Other Contact Number --}}
<div class="col-md-4 col-sm-12 form-group">
    <label class="control-label">Other Contact Number</label>
    <input type="text" disabled name="other_contact_no" placeholder="Other Contact Number" value="{{ old('other_contact_no', isset($user->details->other_contact_no) ? $user->details->other_contact_no : null) }}" class="form-control">
</div>

{{-- Home Tel. Number --}}
<div class="col-md-4 col-sm-12 form-group">
    <label class="control-label">Home Tel. Number</label>
    <input type="text" disabled name="home_tel_no" placeholder="Home Tel. Number" value="{{ old('home_tel_no', isset($user->details->home_tel_no) ? $user->details->home_tel_no : null) }}" class="form-control">
</div>

{{-- Spouse Tel. Number --}}
<div class="col-md-4 col-sm-12 form-group">
    <label class="control-label">Spouse Tel. Number</label>
    <input type="text" disabled name="spouse_tel_no" placeholder="Spouse Tel. Number" value="{{ old('spouse_tel_no', isset($user->details->spouse_tel_no) ? $user->details->spouse_tel_no : null) }}" class="form-control">
</div>

{{-- Province Tel. Number --}}
<div class="col-md-4 col-sm-12 form-group">
    <label class="control-label">Province Tel. Number</label>
    <input type="text" disabled name="province_tel_no" placeholder="Province Tel. Number" value="{{ old('province_tel_no', isset($user->details->province_tel_no) ? $user->details->province_tel_no : null) }}" class="form-control">
</div>

{{-- Fax Number --}}
<div class="col-md-4 col-sm-12 form-group">
    <label class="control-label">Fax Number</label>
    <input type="number" disabled name="fax_no" placeholder="Fax Number" value="{{ old('fax_no', isset($user->details->fax_no) ? $user->details->fax_no : null) }}" class="form-control">
</div>

{{-- Business Fax/Tel Number --}}
<div class="col-md-4 col-sm-12 form-group">
    <label class="control-label">Business Tel/Fax Number</label>
    <input type="text" disabled name="business_no" placeholder="Business Tel/Fax Number" value="{{ old('business_no', isset($user->details->business_no) ? $user->details->business_no : null) }}" class="form-control">
</div>
<!-- End Contact Number -->

<!-- Start ID  -->
<div class="clearfix"></div>
<div class="col-md-12"><legend style=color:#019541;>ID</legend></div>
<label>Primary</label>
<div class="clearfix"></div>

<div class="row">

    {{-- National ID --}}
    <div class="col-md-4 col-sm-12 form-group">
        <label class="control-label">National ID</label>
        <input type="number" disabled name="national_id" placeholder="National ID" value="{{ old('national_id', isset($user->details->national_id) ? $user->details->national_id : null ) }}" class="form-control" >
    </div>

    {{-- TIN --}}
    <div class="col-md-4 col-sm-12 form-group">
        <label class="control-label">{{ Lang::get('members.tin') }}</label>
        <input type="text" disabled name="tin" placeholder="{{ Lang::get('members.tin') }}" value="{{ old('tin', isset($user->details->tin) ? $user->details->tin : null) }}" class="form-control">
    </div>

    {{-- GSIS --}}
    <div class="col-md-4 col-sm-12 form-group">
        <label class="control-label">{{ Lang::get('members.gsis') }}</label>
        <input type="text" disabled name="gsis" placeholder="{{ Lang::get('members.gsis') }}" value="{{ old('gsis', isset($user->details->gsis) ? $user->details->gsis : null) }}" class="form-control">
    </div>

    {{-- SSS --}}
    <div class="col-md-4 col-sm-12 form-group">
        <label class="control-label">{{ Lang::get('members.sss') }}</label>
        <input type="text" disabled name="sss" placeholder="{{ Lang::get('members.sss') }}" value="{{ old('sss', isset($user->details->sss) ? $user->details->sss : null) }}" class="form-control">
    </div>

</div>

<div class="clearfix"></div>
<label>Secondary</label>
<div class="clearfix"></div>

{{-- Secondary Senior --}}
<div class="col-md-2 col-sm-12 form-group">
    <label class="control-label">Senior</label>
    <input type="text" disabled name="senior" placeholder="Senior" value="{{ old('senior', isset($user->details->senior) ? $user->details->senior : null) }}" class="form-control">
</div>

{{-- Voters --}}
<div class="col-md-2 col-sm-12 form-group">
    <label class="control-label">Voters</label>
    <input type="text" disabled name="voters" placeholder="Voters" value="{{ old('voters', isset($user->details->voters) ? $user->details->voters : null) }}" class="form-control">
</div>

{{-- PhilHealth --}}
<div class="col-md-2 col-sm-12 form-group">
    <label class="control-label">PhilHealth</label>
    <input type="text" disabled name="philhealth" placeholder="PhilHealth" value="{{ old('sphilhealthss', isset($user->details->philhealth) ? $user->details->philhealth : null) }}" class="form-control">
</div>

{{-- Pag-ibig --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">Pag-ibig</label>
    <input type="text" disabled name="pagibig" placeholder="Pag-ibig" value="{{ old('pagibig', isset($user->details->pagibig) ? $user->details->pagibig : null) }}" class="form-control">
</div>

{{-- Drivers License --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">Drivers License</label>
    <input type="text" disabled name="drivers_license" placeholder="Drivers License" value="{{ old('drivers_license', isset($user->details->drivers_license) ? $user->details->drivers_license : null) }}" class="form-control">
</div>
<!-- END ID  -->

<!-- Start SPOUSE INFORMATION -->
<div class="clearfix"></div>
<div class="col-md-12"><legend style=color:#019541;>SPOUSE INFORMATION</legend></div>
<div class="clearfix"></div>

{{-- Spouse Last Name --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">Last Name</label>
    <input type="text" disabled name="s_last_name" placeholder="Last Name" value="{{ old('s_last_name', isset($user->details->s_last_name) ? $user->details->s_last_name : null) }}" class="form-control">
</div>

{{-- Spouse First Name --}}
<div class="col-md-3 col-sm-12 form-group">
    {{-- {{ validationError($errors, 's_first_name') }} --}}
    <label class="control-label">First Name</label>
    <input type="text" disabled name="s_first_name" placeholder="First Name" value="{{ old('s_first_name', isset($user->details->s_first_name) ? $user->details->s_first_name : null) }}" class="form-control">
</div>

{{-- Spouse Middle Name --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">Middle Name</label>
    <input type="text" disabled name="s_middle_name" placeholder="Middle Name" value="{{ old('s_middle_name', isset($user->details->s_middle_name) ? $user->details->s_middle_name : null) }}" class="form-control">
</div>

{{-- Spouse Suffix --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">Suffix</label>
    <input type="text" disabled name="s_suffix" placeholder="Suffix" value="{{ old('s_suffix', isset($user->details->s_suffix) ? $user->details->s_suffix : null) }}" class="form-control">
</div>

{{-- Spouse Gender --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">Gender</label>
    <input type="text" disabled name="s_gender" placeholder="Gender" value="{{ old('s_gender', isset($user->details->s_gender) ? $user->details->s_gender : null) }}" class="form-control">
</div>

{{-- Spouse Birth Date --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">Birth Date</label>
    <input type="text" disabled name="s_birth_date" placeholder="0000-00-00" value="{{ old('s_birth_date', isset($user->details->s_birth_date) ? $user->details->s_birth_date : '0000-00-00') }}" class="form-control">
</div>

{{-- Spouse Occupation --}}
<div class="col-md-6 col-sm-12 form-group">
    <label class="control-label">Occupation</label>
    <input type="text" disabled name="s_occupation" placeholder="Occupation" value="{{ old('s_occupation', isset($user->details->s_occupation) ? $user->details->s_occupation : null) }}" class="form-control">
</div>

{{-- Spouse Educational Attainment --}}
<div class="col-md-6 col-sm-12 form-group">
    <label class="control-label">Educational Attainment</label>
    <input type="text" disabled name="s_educational_attainment" placeholder="Educational Attainment" value="{{ old('s_educational_attainment', isset($user->details->s_educational_attainment) ? $user->details->s_educational_attainment : null) }}" class="form-control">
</div>

{{-- Spouse Degree/Course --}}
<div class="col-md-6 col-sm-12 form-group">
    <label class="control-label">Degree/Course</label>
    <input type="text" disabled name="s_degree" placeholder="Degree/Course" value="{{ old('s_degree', isset($user->details->s_degree) ? $user->details->s_degree : null) }}" class="form-control">
</div>

<!-- End SPOUSE INFORMATION -->

<!-- Start PARENTS INFORMATION -->
<div class="clearfix"></div>
<div class="col-md-12"><legend style=color:#019541;>PARENTS INFORMATION</legend></div>
<label>Father's Name</label>
<div class="clearfix"></div>

{{-- Father Last Name --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">Last Name</label>
    <input type="text" disabled name="f_last_name" placeholder="Last Name" value="{{ old('f_last_name', isset($user->details->f_last_name) ? $user->details->f_last_name : null) }}" class="form-control">
</div>

{{-- Father First Name --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">First Name</label>
    <input type="text" disabled name="f_first_name" placeholder="First Name" value="{{ old('f_first_name', isset($user->details->f_first_name) ? $user->details->f_first_name : null) }}" class="form-control">
</div>

{{-- Father Middle Name --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">Middle Name</label>
    <input type="text" disabled name="f_middle_name" placeholder="Middle Name" value="{{ old('f_middle_name', isset($user->details->f_middle_name) ? $user->details->f_middle_name : null) }}" class="form-control">
</div>

{{-- Father Suffix --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">Suffix</label>
    <input type="text" disabled name="f_suffix" placeholder="Suffix" value="{{ old('f_suffix', isset($user->details->f_suffix) ? $user->details->f_suffix : null) }}" class="form-control">
</div>

<div class="clearfix"></div>
<label>Mothers's Maiden Name</label>
<div class="clearfix"></div>

{{-- Mother Last Name --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">Last Name</label>
    <input type="text" disabled name="m_last_name" placeholder="Last Name" value="{{ old('m_last_name', isset($user->details->m_last_name) ? $user->details->m_last_name : null) }}" class="form-control">
</div>

{{-- Mother First Name --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">First Name</label>
    <input type="text" disabled name="m_first_name" placeholder="First Name" value="{{ old('m_first_name', isset($user->details->m_first_name) ? $user->details->m_first_name : null) }}" class="form-control">
</div>

{{-- Mother Middle Name --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">Middle Name</label>
    <input type="text" disabled name="m_middle_name" placeholder="Middle Name" value="{{ old('m_middle_name', isset($user->details->m_middle_name) ? $user->details->m_middle_name : null) }}" class="form-control">
</div>

{{-- Mother Suffix --}}
<div class="col-md-3 col-sm-12 form-group">
    <label class="control-label">Suffix</label>
    <input type="text" disabled name="m_suffix" placeholder="Suffix" value="{{ old('m_suffix', isset($user->details->m_suffix) ? $user->details->m_suffix : null) }}" class="form-control">
</div>

<!-- END PARENTS INFORMATION -->

<div class="clearfix"></div>
<div class="col-md-12"><legend style=color:#019541;>BANK INFORMATION</legend></div>
<div class="clearfix"></div>

{{-- Bank Information --}}

{{-- Bank Name --}}
<div class="form-group">
    <label class="control-label">Bank Name</label>
    <input type="text" disabled name="bank_name" placeholder="Bank Name" value="{{ old('bank_name', isset($user->details->bank_name) ? $user->details->bank_name : null) }}" class="form-control">
</div>

{{-- Account Name --}}
<div class="form-group">
    <label class="control-label">Account Name</label>
    <input type="text" disabled name="bank_account_name" placeholder="Account Name" value="{{ old('bank_account_name', isset($user->details->account_name) ? $user->details->account_name : null) }}" class="form-control">
</div>

{{-- Account Number --}}
<div class="form-group">
    <label class="control-label">Account Number</label>
    <input type="text" disabled name="bank_account_number" placeholder="Account Number" value="{{ old('bank_account_number', isset($user->details->account_number) ? $user->details->account_number : null) }}" class="form-control">
</div>

{{-- Username --}}
<div class="form-group">
    {{-- {{ validationError($errors, 'username') }} --}}
    <label class="control-label">Username</label>
    <input type="text" disabled name="username" placeholder="Username" value="{{ old('username', isset($user->username) ? $user->username : null) }}" class="form-control" readonly>
</div>

{{-- Change Password --}}

{{-- Password --}}
<div class="form-group">
    <label class="control-label">{{ ($id > 0) ? 'Change Password' : 'New Password' }}</label>
    <input type="password" disabled name="password" placeholder="{{ ($id > 0) ? 'Change Password' : 'New Password' }}" value="" class="form-control">
</div>

{{-- Confirm Password --}}
<div class="form-group">
    <label class="control-label">Confirm Password</label>
    <input type="password" disabled name="password_confirmation" placeholder="Confirm Password" value="" class="form-control">
</div>

<div class="clearfix"></div>
<div class="col-md-12"><legend style=color:#019541;>Other Information</legend></div>
<div class="clearfix"></div>

{{-- Other Source of Income --}}
<div class="form-group">
    <label class="control-label">Other source of income</label>
    <input type="text" disabled name="other_income" placeholder="Other source of income" value="" class="form-control">
</div>

{{-- Contact Person in Philippines --}}
<div class="form-group">
  <label class="control-label">Contact Person in Philippines</label>
  <input type="text" disabled name="contact_person" placeholder="Contact Person" value="" class="form-control">
</div>

{{-- Address of Contact Person --}}
<div class="form-group">
    <label class="control-label">Address of Contact Person</label>
    <input type="text" disabled name="address_contact_person" placeholder="Address" value="" class="form-control">
</div>    

<div class="row">
    {{-- Anual Salary (Peso) --}}
    <div class="col-xs-6 col-sm-6 col-md-6">
        <label>Annual Salary (Peso)</label>
        {{ Form::email('employer', '', [
            'class' => 'form-control',
            'disabled' => true,
        ]) }}
    </div>
    {{-- Field of Specialization --}}
    <div class="col-xs-6 col-sm-6 col-md-6">
          <label>Field Of Specialization</label>
          {{ Form::email('country', '', [
              'class' => 'form-control',
            'disabled' => true,
          ]) }}
    </div>
</div>
    {{-- Reason of Joining --}}
    <div class="form-group">
        <label class="control-label">Reason of Joining</label>
        <input type="text" disabled name="reason_of_join" placeholder="Reason of Joining" value="" class="form-control">
    </div>      
    <h5>
        <p style="color:black;">I hereby apply for membership in the Cebu Peoples Multi-Purpose Cooperative and agree to faithfully obey its rules and regulations as set down in its by-laws and amendments thereof, or elsewhere, and the decisions of the general membership as well as those of the board of directors.<br>
        <br>
        Upon submission of the application for membership, I have also paid the amount of Five Hundred Pesos (P500.00) as membership fee for a Regular member, which is refundable in case this application is not approved.
        <br>
        <br>
        <b> I also pledge the following:</b> (Minimum of 4 shares (4,000 Php). Maximum shares of not more than 10% of total subscribed shares capital (400,000 Php)</p></h5>
        <br>
    {{-- Type of Membership --}}
    <div class="form-group">
        <label>Type of Membership</label>
        <select class="sel" name="state"  style="width:400px;" disabled>
            <option value="-1"> Please select membership type</option>
            <option value="1"> (REGULAR)- Minimum Common Subscribe Shares</option>
            <option value="2">(ASSOCIATE)- Minimum Preferred Subscribe Shares</option>
        </select>
    </div>
    
    <br>
    <br>
    <br>
<p style="color:black;"><b>REGULAR MEMBER:</b> One-Time Membership Fee (P500.00), Minimum Subscribe Shares = 4 (P4,000.00), Max Subscribe Shares=400 (P400,000.00), Can Vote and Can be Voted on during election of BOD, Election and Audit Committees.<br><br>
        <b>ASSOCIATE MEMBER:</b> One-Time Membership Fee (P100.00). Minimum Subscribe Shares = 1 (P1,000.00), Maximum Subscribe Shares = 400 (P400,000.00), Cannot Vote and Cannot be Voted on.</p>

    <div class="form-group">
        <label class="control-label">1. Subscribe Shares (Par value 1 share = P1,000)</label>
        {{ Form::text('subscribe_share', '', [
            'class' => 'form-control',
            'disabled' => true,
        ]) }}
    </div>
    <div class="form-group">
        <label class="control-label">2. Payment of Subscribe Shares</label><br>
        {{ Form::radio('Married', '', [
            'class' => 'form-control',   
            'disabled' => true,
        ]) }}
        <label for="married">Full Payment</label><br>
        {{ Form::radio('Married', '', [
            'class' => 'form-control',
            'disabled' => true,
        ]) }}
        <label for="">Partial payment but with initial payment equivalent to 25% of the subscribes shares</label><br>
    </div>
    <br>
                
    <div class="form-group">
        <label class="control-label">3. For partial payment, I promise to pay the balance of my subscription in equal installments within one year</label><br>
        {{ Form::radio('Married', '', [
            'class' => 'form-control', 
            'disabled' => true,
        ]) }} 
    <label for="married">Montly</label>
        {{ Form::radio('Married', '', [
            'class' => 'form-control',
            'disabled' => true,
        ]) }}
    <label for="">Quarterly</label>
        {{ Form::radio('Married', '', [
            'class' => 'form-control',
            'disabled' => true,
        ]) }} 
    <label for="">Semi-annual</label>
        {{ Form::radio('Married', '', [
            'class' => 'form-control',
            'disabled' => true,
        ]) }} 
    <label for="">Annual</label>
    </div>

    <br><br><br>
    <p> I hearby certify that above information are true and correct.</p>         
<script>

    $(document).ready(function(){

        setTimeout(function(){
            $('.alert-profile').addClass('hide');
        }, 1000);

        setTimeout(function(){
            $('.alert-success').addClass('hide');
        }, 3000);


    });

    var x, i, j, selElmnt, a, b, c;
/* Look for any elements with the class "custom-select": */
x = document.getElementsByClassName("custom-select");
for (i = 0; i < x.length; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  /* For each element, create a new DIV that will act as the selected item: */
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /* For each element, create a new DIV that will contain the option list: */
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < selElmnt.length; j++) {
    /* For each option in the original select element,
    create a new DIV that will act as an option item: */
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /* When an item is clicked, update the original select box,
        and the selected item: */
        var y, i, k, s, h;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        h = this.parentNode.previousSibling;
        for (i = 0; i < s.length; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            for (k = 0; k < y.length; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
    /* When the select box is clicked, close any other select boxes,
    and open/close the current select box: */
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
}

function closeAllSelect(elmnt) {
  /* A function that will close all select boxes in the document,
  except the current select box: */
  var x, y, i, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  for (i = 0; i < y.length; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < x.length; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}

/* If the user clicks anywhere outside the select box,
then close all select boxes: */
document.addEventListener("click", closeAllSelect);
</script>


<style>
.custom-select {
  position: relative;
  font-family: Raleway, sans-serif;
  font:bolder;
}

.custom-select select {
  display: none; /*hide original SELECT element: */
}

.select-selected {
  background-color: #EEEEEE;
  border:#EEEEEE;
}

/* Style the arrow inside the select element: */
.select-selected:after {
  position: absolute;
  content: "";
  top: 14px;
  right: 10px;
  width: 0;
  height: 0;
  border: 6px solid transparent;
  border-color: #fff transparent transparent transparent;
}

/* Point the arrow upwards when the select box is open (active): */
.select-selected.select-arrow-active:after {
  border-color: transparent transparent #fff transparent;
  top: 7px;
}

/* style the items (options), including the selected item: */
.select-items div,.select-selected {
  color: black;
  padding: 8px 16px;
  border: black;
  border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
  cursor: pointer;
}

/* Style items (options): */
.select-items {
  position: absolute;
  background-color:white;
  top: 100%;
  left: 0;
  right: 0;
  z-index: 99;
}

/* Hide the items when the select box is closed: */
.select-hide {
  display: none;
}

.select-items div:hover, .same-as-selected {
  background-color: rgba(0, 0, 0, 0.1);
}
    input[type="text"]{
        font-weight: bold;
        color: #202328 !important;
    }
    input[type="text"]::placeholder{
        font-weight: 100;
        color: #6c6c6d;
    }
    .red-background{
        background-color: #ffdddd;
    }

    .red-color{
        color: #B12128 !important;
    }

</style>
