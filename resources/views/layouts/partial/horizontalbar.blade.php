<div id="mySidenav" class="sidenav">
    @if($theUser->role_type == 'member')
    <button class="dropdown-btn"><span class="fas fa-tachometer-alt " style="color:#019541" ></span>   Dashboard</button>
    <ul class="dropdown-container">
        <li {{ ($segment2 == MEMBERS_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/members') }}">{{ Lang::get('labels.members') }}</a></li>
    </ul>
</div>
@elseif($theUser->role_type != 'accounting')
    @if($theUser->role_type == 'vp')
    <button class="dropdown-btn"><span class="fas fa-tachometer-alt"></span>   Dashboard</button>
    <ul class="dropdown-container">
        @if (hasAccess(DASHBOARD_MODULE))
            <li {{ ($segment2 == DASHBOARD_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/dashboard') }}">{{ Lang::get('labels.summary') }}</a></li>
        @endif

        @if (hasAccess(TOP_EARNERS_MODULE))
            <li {{ ($segment2 == TOP_EARNERS_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/top-earners') }}">{{ Lang::get('labels.top_earners') }}</a></li>
        @endif
            <li {{ ($segment2 == 'announcement') ? 'class="active"' : null }}><a href="{{ url('admin/announcement') }}">Announcement</a></li>
    </ul>
    @if (hasAccess(PRODUCTS_MODULE) or hasAccess(PURCHASE_CODES_MODULE) or hasAccess(UNILEVEL_MODULE))
    <button class="dropdown-btn"><span class="fas fa-tags fa-3x"></span>  Products</button>
    <ul class="dropdown-container">
        @if (hasAccess(PRODUCTS_MODULE))
            <li {{ ($segment3 == '') ? 'class="active"' : null }}><a href="{{ url('admin/products') }}">{{ Lang::get('labels.product_list') }}</a></li>
        @endif
        @if (hasAccess(PURCHASE_CODES_MODULE))
            <li {{ ($segment3 == PURCHASE_CODES_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/products/purchase-codes') }}">{{ Lang::get('labels.purchase_codes') }}</a></li>
        @endif
        @if (hasAccess(UNILEVEL_MODULE))
            <li {{ ($segment3 == UNILEVEL_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/products/unilevel') }}">{{ Lang::get('labels.unilevel_settings') }}</a></li>
        @endif
    </ul>
    @endif
@else
    <img class="img" src="{{ url('public/img/login/logo.png') }}"width=100% alt="">  
    <hr>
    @if (hasAccess(DASHBOARD_MODULE)
    or hasAccess(ACTIVATION_CODES_MODULE)
    or hasAccess(MEMBERS_MODULE)
    or hasAccess(FUNDING_MODULE)
    or hasAccess(WITHDRAWALS_MODULE)
    or hasAccess(TOP_EARNERS_MODULE)
    or hasAccess(ADMINISTRATORS_MODULE))
    <!--button class="dropdown-btn"><span class="fas fa-tachometer-alt " ></span>   Dashboard</button-->
    
    <!--ul class="dropdown-container"-->
        @if (hasAccess(DASHBOARD_MODULE))
            <!--li {{ ($segment2 == DASHBOARD_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/dashboard') }}">{{ Lang::get('labels.summary') }}</a></li-->
            <a href="{{ url('admin/dashboard') }}"><span class="far fa-file-alt" ></span> {{ Lang::get('labels.summary') }}</a>
        @endif

        @if (hasAccess(ACTIVATION_CODES_MODULE))
            <!--li {{ ($segment2 == ACTIVATION_CODES_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/activation-codes') }}">{{ Lang::get('labels.activation_codes') }}</a></li--> 
            <a href="{{ url('admin/activation-codes') }}"><span class="fas fa-clipboard-list" ></span> {{ Lang::get('labels.activation_codes') }}</a>
        @endif

           <!--li {{ ($segment2 == MEMBERS_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/members') }}">{{ Lang::get('labels.members') }}</a></li--> 
           <a href="{{ url('admin/members') }}"><span class="fas fa-users" ></span> {{ Lang::get('labels.members') }}</a>

           <a href="{{ url('admin/payout-history/income') }}"><span class="fas fa-window-restore" ></span> Payout History</a>

           <a href="{{ url('admin/registration-history') }}"><span class="fas fa-id-card" ></span> Registration History</a>
           <a href="{{ url('admin/payout-history') }}"><span class="fas fa-chart-bar" ></span> Income History</a>
           <a href="{{ url('admin/payout-history/directreferral') }}"><span class="fas fa-window-restore" ></span> Direct Referral History</a>
           <a href="{{ url('admin/payout-history/indirectreferral') }}"><span class="fas fa-window-restore" ></span> Indirect Referral History</a>
          
           <a href="{{ url('admin/company/branch-teller') }}"><span class="fas fa-user-plus" ></span> Teller Account</a>
           <a href="{{ url('admin/tellers') }}"><span class="fas fa-address-card" ></span> Teller Lists</a>
           <a href="{{ url('admin/withdrawals') }}"><span class="fas fa-tasks" ></span> Withdrawal Requests</a>


<!--
        @if (hasAccess(FUNDING_MODULE))
            <li {{ ($segment2 == FUNDING_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/funding') }}">{{ Lang::get('labels.funding') }}</a></li>
        @endif 
        @if (hasAccess(TRANSACTIONS_MODULE))
            <li {{ ($segment2 == TRANSACTIONS_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/transactions') }}">Transactions</a></li>
        @endif
      @if (hasAccess(WITHDRAWALS_MODULE))
            <li {{ ($segment2 == WITHDRAWALS_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/withdrawals') }}">{{ Lang::get('labels.withdrawals') }}</a></li>
        @endif
        @if (hasAccess(TOP_EARNERS_MODULE))
            <li {{ ($segment2 == TOP_EARNERS_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/top-earners') }}">{{ Lang::get('labels.top_earners') }}</a></li>
        @endif 
        @if (hasAccess(ADMINISTRATORS_MODULE))
            <li {{ ($segment2 == ADMINISTRATORS_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/administrators') }}">{{ Lang::get('labels.administrators') }}</a></li>
        @endif
        @if (hasAccess(PAYMENT_HISTORY_MODULE))
            <li {{ ($segment2 == 'flushout-history') ? 'class="active"' : null }}><a href="{{ url('admin/flushout-history') }}">Flushout History</a></li>
            <li {{ ($segment2 == 'maintenance-history') ? 'class="active"' : null }}><a href="{{ url('admin/maintenance-history') }}">Maintenance History</a></li>
            <li {{ ($segment2 == PAYMENT_HISTORY_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/payment-history') }}">{{ Lang::get('labels.payment_history') }}</a></li>
            <li {{ ($segment3 == 'income') ? 'class="active"' : null }}><a href="{{ url('admin/payout-history/income') }}">Weekly Payout History</a></li>

            <li {{ ($segment2 == REGISTRATON_HISTORY_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/registration-history') }}">Registration History</a></li>
            <li {{ (($segment3 != PAYOUT_HISTORY_DR_MODULE && $segment3 != PAYOUT_HISTORY_MB_MODULE && $segment3 != 'income') && $segment2 == PAYOUT_HISTORY_MODULE ) ? 'class="active"' : null }}><a href="{{ url('admin/payout-history') }}">Income History</a></li>

            <li {{ ($segment3 == PAYOUT_HISTORY_DR_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/payout-history/directreferral') }}">Income History Direct Referral</a></li>
            <li {{ ($segment3 == PAYOUT_HISTORY_MB_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/payout-history/matchingbonus') }}">Income History Matching Bonus</a></li>

            <li {{ ($segment2 == 'cd-accounts') ? 'class="active"' : null }}><a href="{{ url('admin/cd-accounts') }}">CD Account History</a></li>

            <li {{ ($segment2 == 'giftcheck-history') ? 'class="active"' : null }}><a href="{{ url('admin/giftcheck-history') }}">Gift Check History</a></li>
            <li {{ ($segment2 == 'points-summary') ? 'class="active"' : null }}><a href="{{ url('admin/points-summary') }}">Points Summary</a></li>
            <li {{ ($segment2 == 'announcement') ? 'class="active"' : null }}><a href="{{ url('admin/announcement') }}">Announcement</a></li>
        @endif-->
    <!--/ul-->
@endif

<!--@if (hasAccess(PRODUCTS_MODULE) or hasAccess(PURCHASE_CODES_MODULE) or hasAccess(UNILEVEL_MODULE))
<button class="dropdown-btn"><span class="fas fa-cogs fa-1x"></span>Services</button>
    <ul class="dropdown-container">
       @if (hasAccess(PRODUCTS_MODULE))
            <li {{ ($segment3 == '') ? 'class="active"' : null }}><a href="{{ url('admin/products') }}">{{ Lang::get('labels.product_list') }}</a></li>
        @endif
        @if (hasAccess(PURCHASE_CODES_MODULE))
            <li {{ ($segment3 == PURCHASE_CODES_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/products/purchase-codes') }}">{{ Lang::get('labels.purchase_codes') }}</a></li>
        @endif
        @if(hasAccess(PURCHASE_CODES_INVENTORY_MODULE))
            <li {{ $segment3 ==  PURCHASE_CODES_INVENTORY_MODULE ? 'class="active"' : null }}><a href="{{ url('admin/products/purchase-codes-inventory') }}">Purchase Codes Inventory</a></li>
        @endif
        @if (hasAccess(UNILEVEL_MODULE))
            <li {{ ($segment3 == UNILEVEL_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/products/unilevel') }}">{{ Lang::get('labels.unilevel_settings') }}</a></li>
        @endif
        {{-- Redundant Binary --}}
        @if (hasAccess(REDUNDANT_BINARY_MODULE))
            <li {{ ($segment3 == REDUNDANT_BINARY_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/products/redundant-binary-settings') }}">Redundant Binary Settings</a></li>
        @endif
        
    </ul>
@endif-->

{{-- Transaction --}}

<!--a href="{{ url('admin/triptransactions/') }}"><span class="far fa-file-alt" ></span>    Transactions</a-->
<!--@if (hasAccess(COMPANY_DETAILS_MODULE) or hasAccess(CONNECTIONS_MODULE) or hasAccess(CODE_SETTINGS_MODULE))
<button class="dropdown-btn"><span class="fas fa-university" ></span>   Products</button>
<ul class="dropdown-container">
        @if (hasAccess(COMPANY_DETAILS_MODULE))
            <li {{ ($segment2 == COMPANY_DETAILS_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/company') }}">{{ Lang::get('labels.company_details') }}</a></li>
        @endif
        @if (hasAccess(CONNECTIONS_MODULE))
            <li {{ ($segment2 == CONNECTIONS_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/connections') }}">{{ Lang::get('labels.connections') }}</a></li>
        @endif
        @if (hasAccess(CODE_SETTINGS_MODULE))
            <li {{ ($segment2 == 'settings') ? 'class="active"' : null }}><a href="{{ url('admin/settings') }}">{{ Lang::get('labels.settings') }}</a></li>
        @endif
        @if (hasAccess(PAYPAL_MODULE))
            <li {{ ($segment3 == PAYPAL_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/paypal/settings') }}">{{ Lang::get('labels.paypal_settings') }}</a></li>
        @endif
</ul>
@endif -->

<!-- Income and Payout -->
<!--
@if (hasAccess(COMPENSATION_INCOME_MODULE) or hasAccess(PAIRING_MODULE) or hasAccess(WITHDRAWAL_SETTINGS_MODULE))
    <button class="dropdown-btn"><span class="fas fa-money-bill-alt" ></span>Income and Payout</button>
    <ul class="dropdown-container">
            @if (hasAccess(COMPENSATION_INCOME_MODULE))
                <li {{ ($segment2 == COMPENSATION_INCOME_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/income') }}">{{ Lang::get('labels.income_settings') }}</a></li>
        @endif
        @if (hasAccess(PAIRING_MODULE))
            <li {{ ($segment2 == PAIRING_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/pairing') }}">{{ Lang::get('labels.pairing_settings') }}</a></li>
        @endif
        @if (hasAccess(WITHDRAWAL_SETTINGS_MODULE))
            <li {{ ($segment2 == WITHDRAWAL_SETTINGS_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/withdrawal-settings') }}">{{ Lang::get('labels.withdrawal_settings') }}</a></li>
        @endif
    </ul>
@endif
-->
<!-- Mailing -->
<!-- 
@if (hasAccess(MAIL_TEMPLATES_MODULE))
    <button class="dropdown-btn"><span class="fas fa-envelope" ></span>Mailing</button>
    <ul class="dropdown-container">
                <li {{ ($segment2 == MAIL_TEMPLATES_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/mail-templates') }}">{{ Lang::get('labels.mail_templates') }}</a></li>
        </ul>
        
    </div>
@endif
-->

@endif
    @else
        <div style="width: 800px; margin: 0 auto;">
            <div class="h-menu col-centered" style="width: 150px; position: relative;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fas fa-tachometer-alt fa-3x"></i>
                    <div>{{ Lang::get('labels.dashboard') }}</div>
                </a>
                <ul class="h-sub-menu dropdown-menu animated flipInX">
                    <li {{ ($segment2 == REGISTRATON_HISTORY_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/registration-history') }}">Registration History</a></li>
                    @if (hasAccess(WITHDRAWALS_MODULE))
                        <li {{ ($segment2 == WITHDRAWALS_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/withdrawals') }}">{{ Lang::get('labels.withdrawals') }}</a></li>
                    @endif
                    <li {{ ($segment2 == ACTIVATION_CODES_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/activation-codes') }}">{{ Lang::get('labels.activation_codes') }}</a></li>
                    <li {{ ($segment3 == 'income') ? 'class="active"' : null }}><a href="{{ url('admin/payout-history/income') }}">Weekly Payout History</a></li>
                    <li {{ (($segment3 != PAYOUT_HISTORY_DR_MODULE && $segment3 != PAYOUT_HISTORY_MB_MODULE) && $segment2 == PAYOUT_HISTORY_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/payout-history') }}">Income History</a></li>

                    <!--li {{ ($segment3 == PAYOUT_HISTORY_DR_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/payout-history/directreferral') }}">Income History Direct Referral</a></li>
                    
                    <li {{ ($segment3 == PAYOUT_HISTORY_MB_MODULE) ? 'class="active"' : null }}><a href="{{ url('admin/payout-history/matchingbonus') }}">Income History Matching Bonus</a></li>
                    <li {{ ($segment2 == 'cd-accounts') ? 'class="active"' : null }}><a href="{{ url('admin/cd-accounts') }}">CD Account History</a></li>
                    <li {{ ($segment2 == 'giftcheck-history') ? 'class="active"' : null }}><a href="{{ url('admin/giftcheck-history') }}">Gift Check History</a></li>
                    <li {{ ($segment2 == 'maintenance-history') ? 'class="active"' : null }}><a href="{{ url('admin/maintenance-history') }}">Maintenance History</a></li-->
                </ul>
                
            </div>
        </div>
    @endif
</div>

<style>.sidenav {
    height: 100%;
    width: 200px;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color:white;
    overflow-x: hidden;
    padding-top: 20px;
  
    
  }
  
  /* Style the sidenav links and the dropdown button */
  .sidenav a, .dropdown-btn {
    padding: 10px 10px 10px 29px;
    text-decoration: none;
    font-size: 16px;
    font-family: Century Gothic;
    color: black;
    display: block;
    border: none;
    background: none;
    width:100%;
    text-align: left;
    cursor: pointer;
    outline: black;
  }
  
  /* On mouse-over */
  .sidenav a:hover, .dropdown-btn:hover {
    color: black;
  }
  
  /* Main content */
  .main {
    margin-left: 200px; /* Same as the width of the sidenav */
    /* font-size: 13px; Increased text to enable scrolling */
    padding: 0px 10px;
  }
  
  /* Add an active class to the active dropdown button */
  .actives {
    color: black;
  }
  
  /* Dropdown container (hidden by default). Optional: add a lighter background color and some left padding to change the design of the dropdown content */
  .dropdown-container {
    display: none;
    background-color:white;
    padding-left: 8px;
  }
  
  /* Optional: Style the caret down icon */
  .fa-caret-down {
    float: right;
    padding-right: 8px;
  }
  </style>
  
        
<script>
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;
        
    for (i = 0; i < dropdown.length; i++) {
        dropdown[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var dropdownContent = this.nextElementSibling;
            if (dropdownContent.style.display === "block") {
                dropdownContent.style.display = "none";
            } else {
                dropdownContent.style.display = "block";
            }
        });
    }
</script>

        