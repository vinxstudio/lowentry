

<div id="mySidenav" class="sidenav">
    <img class="img" src="{{ url('public/img/login/logo.png') }}"width=100% alt="">  
    <hr>
    <a href="{{ url('member/dashboard') }}"><span class="fas fa-tachometer-alt"></span> Dashboard</a>
  <!--button class="dropdown-btn"><span class="fas fa-tachometer-alt"></span>   Dashboard</button-->
  
	  
			<!--ul class="dropdown-container">
					<li {{ ($segment2 == 'dashboard') ? 'class="actives"' : null }}><a href="{{ url('member/dashboard') }}">{{ Lang::get('labels.summary') }}</a></li>
					{{-- <li {{ ($segment2 == 'investment') ? 'class="actives"' : null }}><a href="{{ url('member/investments') }}">{{ Lang::get('labels.transaction_logs') }}</a></li> --}}
					<li {{ ($segment2 == 'payout-history') ? 'class="actives"' : null }}><a href="{{ url('member/payout-history') }}">Income History</a></li-->
					<!--li {{ ($segment3 == 'history') ? 'class="actives"' : null }}><a href="{{ url('member/giftcheck/history') }}">Giftcheck History</a></li-->
					<!--li {{ ($segment2 == 'weeklypayout') ? 'class="actives"' : null }}><a href="{{ url('member/weeklypayout') }}">Weekly Payout History</a></li-->
					<!--li {{ ($segment2 == 'summary') ? 'class="actives"' : null }}><a href="{{ url('member/summary') }}">Income/Encashment Summary</a></li>
				</ul-->

<?php $production = true;  ?>
@if($production)
<a href="{{url('member/network')}}"><span class="fas fa-sitemap"></span> Network</a>
<a href="{{url('member/investments')}}"><span class="fas fa-id-card"></span> Codes</a>
<!--a href="{{ url('member/programServices/') }}"><span class="fas fa-cogs"></span>    Services</a!-->
	{{-- <a href="{{ url('member/programServices/') }}"> <span class="fa fa-shopping-bag"></span>
		<div>{{ Lang::get('labels.programServices') }}</div>
	</a> --}}
	<!--a href="{{ url('member/transactions/') }}"><span class="far fa-file-alt"></span>    Transactions</a-->
		
		{{-- <div>{{ Lang::get('labels.transactions') }}</div> --}}
	</a>
@endif 
{{-- <button class="dropdown-btn"><span class="fa fa-shopping-cart"></span>        Purchases</button> --}}

	</a> 
	<ul class="dropdown-container">
		<li {{ ($segment3 == 'buy') ? 'class="active"' : null }}><a href="{{ url('member/programServices/eCommerce') }}">{{ Lang::get('labels.buy_product') }}</a></li>
		<li {{ ($segment3 == 'encode') ? 'class="active"' : null }}><a href="{{ url('member/purchases/encode') }}">{{ Lang::get('labels.encode') }}</a></li>
		{{-- January 13, 2019 --}}
		{{-- remove encode product code in member --}}
		{{-- <li {{ ($segment3 == 'encode-product-codes') ? 'class="active"' : null }}><a href="{{ url('member/purchases/encode-product-codes') }}">Encode Product Codes</a></li> --}}
		<li {{ ($segment3 == 'list') ? 'class="active"' : null }}><a href="{{ url('member/purchases/list') }}">{{ Lang::get('labels.purchase_history') }}</a></li>
	</ul>
	<button class="dropdown-btn"><span class="far fa-credit-card"></span>   Withdrawals</button>

		<ul class="dropdown-container">
			<li {{ ($segment3 == 'request') ? 'class="active"' : null }}><a href="{{ url('member/withdrawals/request') }}">{{ Lang::get('labels.request_withdrawal') }}</a></li>
			<li {{ ($segment3 == 'pending') ? 'class="active"' : null }}><a href="{{ url('member/withdrawals/pending') }}">{{ Lang::get('labels.pending_withdrawal') }}</a></li>
			<li {{ ($segment3 == 'history') ? 'class="active"' : null }}><a href="{{ url('member/withdrawals/history') }}">{{ Lang::get('labels.history_withdrawal') }}</a></li>
		</ul>
		{{-- <a href=""><span class="fa fa-shopping-bag"></span>        Support</a> --}}
      </a>
</div>
<style>

  
.sidenav {
  height: 100%;
  width: 200px;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color:white;
  overflow-x: hidden;
  padding-top: 20px;

  
}

/* Style the sidenav links and the dropdown button */
.sidenav a, .dropdown-btn {
  padding: 10px 10px 10px 29px;
  text-decoration: none;
  font-size: 18px;
  font-family: Century Gothic;
  color: black;
  display: block;
  border: none;
  background: none;
  width:100%;
  text-align: left;
  cursor: pointer;
  outline: black;
}

/* On mouse-over */
.sidenav a:hover, .dropdown-btn:hover {
  color: black;
}

/* Main content */
.main {
  margin-left: 200px; /* Same as the width of the sidenav */
  /* font-size: 13px; Increased text to enable scrolling */
  padding: 0px 10px;
}

/* Add an active class to the active dropdown button */
.actives {
  color: black;
}

/* Dropdown container (hidden by default). Optional: add a lighter background color and some left padding to change the design of the dropdown content */
.dropdown-container {
  display: none;
  background-color:white;
  padding-left: 8px;
}

/* Optional: Style the caret down icon */
.fa-caret-down {
  float: right;
  padding-right: 8px;
}
</style>


<script>
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var dropdownContent = this.nextElementSibling;
    if (dropdownContent.style.display === "block") {
      dropdownContent.style.display = "none";
    } else {
      dropdownContent.style.display = "block";
    }
  });
}
</script>
   
{{-- </body>
</html>  --}}
