@extends('layouts.emailLayout')
@section('content')
   <p>A new member has created an account.</p>
   <p>Please verify by logging into your account</p>
@stop