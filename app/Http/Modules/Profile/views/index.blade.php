@extends($layout)
@section('plugin_css')
    {{ LoadPlugin('dropzone', 'css') }}
    {{ LoadPlugin('sweet-alert', 'css') }}
@stop
@section('plugin_js')
    {{ LoadPlugin('dropzone', 'js') }}
    {{ LoadPlugin('sweet-alert', 'js') }}
    {{ LoadPlugin('dropzoneCallback', 'js') }}
@stop

@section('content')
    <div class="main">
        <div class="col-lg-3 col-md-3 col-sm-4">
            <div class="panel rounded shadow">
                <div class="panel-body photo-panel">
                    <div class="inner-all">
                        <ul class="list-unstyled">
                            <li class="text-center">
                                <img class="img-circle img-bordered-primary" width="100" height="100" src="{{ url($theUser->details->photo) }}" alt="{{ $theUser->details->full_name }}">
                            </li>
                            <li class="text-center">
                                <h4 class="text-capitalize">{{ $theUser->role }}</h4>
                                {{-- <p class="text-muted"></p></p> --}}
                                <p class="text-muted text-capitalize">{{ ($theUser->role != 'member') ? strtoupper($theUser->role) : $theUser->details->full_name }}</p>
                                <p class="text-muted text-capitalize">{{ ($theUser->role != 'member') ? strtoupper($theUser->role) : $theUser->membership_id }}</p>
                                <p class="text-muted text-capitalize"><a href="" class="change-photo" style="color: #019541;">change photo</a></p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body change-photo-panel hidden">
                    <p class="text-muted text-capitalize"><a href="" class="cancel-change-photo">cancel</a></p>
                    {{ Form::open([
                        'class'=>'dropzone',
                        'files'=>'true',
                        'id'=>'dropzone-custom'
                    ]) }}

                        <div class="fallback">
                            <input name="file" type="file" />
                        </div>
                    {{ Form::close() }}
                </div>
            </div><!-- /.panel -->

            @if ($theUser->role == 'member')
            <div class="panel panel-theme rounded shadow">
              {{-- <!--  <div class="panel-heading" style="background:#019541;">
                    <div class="pull-left">
                        {{-- <h3 class="panel-title">{{ Lang::get('profile.bank_details') }}</h3> --}}
                    {{-- </div> --}}
                    {{-- <div class="clearfix"></div> --}}
                {{-- </div>--> --}}
             <!--   <div class="panel-body no-padding rounded">
                    <ul class="list-group no-margin">
                        <li class="list-group-item"><i class="fa fa-bank mr-5"></i> {{ $theUser->details->bank_name }}</li>
                        <li class="list-group-item"><i class="fa fa-bank mr-5"></i> {{ $theUser->details->account_name }}</li>
                        <li class="list-group-item"><i class="fa fa-bank mr-5"></i> {{ $theUser->details->account_number }}</li>
                    </ul>
                </div>-->
            </div>
            @endif
        </div>
        <div class="col-lg-9 col-md-9 col-sm-8">
            <div class="panel panel-theme rounded shadow">
                <div class="panel-heading" style="background: #019541;">
                    <div class="pull-left">
                        <h3 class="panel-title">{{ Lang::get('profile.update') }}</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body rounded">
                    {{ Form::open() }}
                    
                        @include('widgets.advance_profile_fields')

                        <div class="form-group">
                            {{ Form::button('Submit', [
                                'type'=>'submit',
                                'value'=>'update',
                                'name'=>'update',
                                'class'=>'btn btn-theme',
                                'style'=>'background:#019541'
                            ]) }}
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div><!-- /.row -->
@stop