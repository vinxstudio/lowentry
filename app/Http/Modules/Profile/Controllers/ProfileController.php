<?php

/**
 * Created by PhpStorm.
 * User: jomeravengoza
 * Date: 3/11/17
 * Time: 3:53 PM
 */

namespace App\Http\Modules\Profile\Controllers;

use App\Http\AbstractHandlers\MainAbstract;
use App\Models\Company;
use App\Models\Details;
use App\Models\Membership;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use App\Models\UserDetails;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\UserAddress;

class ProfileController extends MainAbstract
{

    protected
        $layout = 'layouts.master',
        $viewpath = 'Profile.views.',
        $user,
        $maxFileSize = 1000000000;

    function __construct()
    {
        parent::__construct();
        $this->user = Auth::user();
        // 1/16/2018
        $type = $this->user->member_type_id;
        $this->theMembership = Membership::find($type);

        if ($this->user->role == 'member') {
            $this->layout = 'layouts.members';
        }

        view()->share([
            'layout' => $this->layout,
            'theUser' => $this->user,
            'company' => Company::find(1),
            'menus' => ($this->user->role == 'member') ? $this->memberMenus : $this->adminMenus,
            'member' => $this->theMembership
        ]);
    }

    function getIndex()
    {
        return view($this->viewpath . 'index')
            ->with([
                'id' => $this->user->id,
                'user' => $this->user
            ]);
    }

    /**
     * Old Update Profile
     */

    // function postIndex(Request $request)
    // {

    //     if ($request->ajax()) {

    //         $result = new \stdClass();
    //         $result->status = 'error';
    //         $result->message = '';

    //         if ($request->hasFile('file')) {

    //             $file = $request->file('file');
    //             $size = $file->getClientSize();
    //             $extension = $file->getClientOriginalExtension();

    //             if ($size >= $this->maxFileSize) {
    //                 $result->message = 'You have exceeded maximum file size allowed.';
    //             } else {
    //                 $newPathName = sprintf('%s%s.%s', time(), $this->user->id, $extension);
    //                 $file->move($this->upload_location, $newPathName);

    //                 $fullPath = sprintf('%s/%s', $this->upload_location, $newPathName);

    //                 $details = Details::find($this->user->user_details_id);

    //                 if (file_exists($details->photo)) {
    //                     unlink($details->photo);
    //                 }

    //                 $details->photo = $fullPath;
    //                 $details->save();
    //                 $result->status = 'success';
    //                 $result->message = 'Photo successfully updated.';
    //             }

    //         }

    //         return response()->json($result);

    //     } else {
    //         $result = new \stdClass();
    //         $result->error = false;
    //         $result->message = '';

    //         $validation = $this->validateUserDetails($request);

    //         if ($validation->fails()) {
    //             $result->error = true;
    //         } else {
    //             $this->saveDetails($this->user->id, $request);
    //             $result->message = Lang::get('messages.saved');
    //         }

    //         return ($result->error) ? redirect($request->segment(1) . '/profile')
    //             ->withInput()
    //             ->withErrors($validation->errors())
    //             ->with('danger', $result->message) : redirect($request->segment(1) . '/dashboard')
    //             ->with('success', $result->message);
    //     }

    // }


    /**
     * New Update Profile
     */

    function postIndex(Request $request)
    {

        $result_status = '';
        $result_message = '';




        $user = auth()->user();

        DB::beginTransaction();
        try {

            $user_details = UserDetails::find($user->user_details_id);

            $user_details->birth_date = $request->birth_date;
            $user_details->birth_place = $request->birth_place;
            $user_details->profession = $request->profession;
            $user_details->gender = $request->gender;
            $user_details->religion = $request->religion;
            $user_details->nationality = $request->nationality;
            $user_details->civil_status = $request->civil_status;
            $user_details->height = $request->height;
            $user_details->weight = $request->weight;
            $user_details->blood_type = $request->blood_type;
            $user_details->user_type = $request->user_type;
            $user_details->save();

    
            DB::commit();
        } catch (\Exception $ex) {

            $result_status = 'error';
            $result_message['message'] = 'Opsss, Something Went Wrong';

            return redirect()->back()->withErrors($result_message)->withInput();

            DB::rollback();
        }

        if ($request->hasFile('file')) {

            $file = $request->file('file');
            $size = $file->getClientSize();
            $extension = $file->getClientOriginalExtension();

            if ($size >= $this->maxFileSize) {

                $result_message['message'] = 'You have exceeded maximum file size allowed';

                return redirect()->back()->withErrors($result_message)->withInput();
            } else {
                $newPathName = sprintf('%s%s.%s', time(), $this->user->id, $extension);
                $file->move($this->upload_location, $newPathName);

                $fullPath = sprintf('%s/%s', $this->upload_location, $newPathName);

                $details = Details::find($this->user->user_details_id);

                if (file_exists($details->photo)) {
                    unlink($details->photo);
                }

                $details->photo = $fullPath;
                $details->save();
            }
        }


        $message = [
            'date_of_application.required' => 'Date of Application is required',
            'password.confirmed' => 'Incorrect Password Confirmation',
            'gender.required' => 'Gender is Required',
            'religion.required' => 'Religion is Required',
            'nationality.required' => 'Nationality is Required',
            'present_street.required' => 'Street Address is Required',
            'present_barangay.required' => 'Banranggay is Required',
            'present_town.required' => 'Town is Required',
            'present_city.required' => 'City is Required',
            'present_zipcode' => 'Zip Copde is Required',
            'user_type' => 'User type is Required'
        ];

        $address = new UserAddress();

        $user_address = $address->single_data($address::UserTable, $user->id);

        DB::beginTransaction();
        try {

            $this->validate($request, [
                'present_street' => 'required',
                'present_barangay' => 'required',
                'present_town' => 'required',
                'present_city' => 'required',
                'present_zipcode' => 'required'
            ], $message);


            if ($user->date_of_application == '0000-00-00 00:00:00') {
                $this->validate($request, ['date_of_application' => 'required'], $message);
                $user->date_of_application = $request->date_of_application;
            }

            $user_address->street = $request->present_street;
            $user_address->barangay = $request->present_barangay;
            $user_address->town = $request->present_town;
            $user_address->city = $request->present_city;
            $user_address->zip_code = $request->present_zipcode;

            $user_address->save();


            // $old_email = auth()->user()->email;

            // if ($old_email != $request->email)
            //     auth()->user()->email = $request->email;

            // if ($request->password != '') {
            //     auth()->user()->password = Hash::make($request->password);
            // }


            $user->save();


            DB::commit();
        } catch (\Exception $ex) {

            $result_status = 'error';
            $result_message['message'] = 'Opsss, Something Went Wrong';

            return redirect()->back()->withErrors($result_message)->withInput();

            DB::rollback();
        }


        $result_status = 'success';
        $result_message = 'Successfully updated profile';

        session()->forget('need_profile_update');

        return redirect()->back()->with([
            'status' => $result_status,
            'message' => $result_message
        ]);
    }
}
