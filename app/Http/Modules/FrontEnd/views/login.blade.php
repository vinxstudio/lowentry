@extends('layouts.loginLayout')
@section('content')

		<div class="container">
			<div class="row">
			  <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
				<div class="card card-signin my-5">
				  <div class="card-body">
						@foreach ($errors->all() as $error)
								<div class="alert alert-danger">
									{{ $error }}
								</div>
						@endforeach
						@if(session()->has('signup_message'))
								<div class="alert alert-success">
										{{ session('signup_message') }}
								</div>
						@endif
						<div class="img">
							 <img class="img" src="{{ url('public/img/login/logo.png') }}" alt="">  
							 <h4 style="color:#074989">MEMBERS LOGIN</h4>
						</div>
						{{ Form::open(
							[
								'class'=>'sign-in form-horizontal rounded no-overflow'
							]
						) }}
						<br>
						<div class="input">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<div class="{{ $errors->first('email') ? 'has-error has-feedback' : null }}">
									<input type="text" class="form-control" placeholder="Email Address" name="email">
									{{ validationError($errors, 'email') }}
								</div>
							</div>
							<div class="input">
								<input type="password" class="form-control" placeholder="{{ Lang::get('labels.password') }}" name="password">
							</div>
		
							<div class="row my-3 text-center">
									<div class="col">
										<div class="form-check">
											<input class="form-check-input" type="checkbox" value="" id="remember-pass">
											<label class="form-check-label" for="remember-pass">Keep me login</label>
										</div>
									</div>
									<!--div class="col">
										<a href="#" class="forgot-pass">Forgot Password?</a>
									</div-->

								</div>
								<div class="button">
								<button type="submit" class="btn btn-dark d-block w-100 rounded-0">LOGIN</button>
								<br>

									<a class="btn btn-dark d-block w-100 rounded-0" href="/auth/sign-up">REGISTER</a>
								</div>

		
					  <hr class="my-4">
					  {{-- <button class="btn btn-lg btn-google btn-block text-uppercase" type="submit"><i class="fab fa-google mr-2"></i> Sign in with Google</button>
					  <button class="btn btn-lg btn-facebook btn-block text-uppercase" type="submit"><i class="fab fa-facebook-f mr-2"></i> Sign in with Facebook</button> --}}
						{{ Form::close() }}
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		</body>
		<style>
	body {background-color: #074989;}
.img {
  left: 0;
  top: 50%;
  text-align: center;
  background: white;

}

.card-signin {
  border: 0;
  box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.1);
}

.card-signin .card-title {
  margin-bottom: 2rem;
  font-weight: 300;
  font-size: 1.5rem;
}

.card-signin .card-body {
  padding: 2rem;

}

.form-signin {
  width: 100%;
}

.form-signin .btn {
  font-size: 50%;
  border-radius: 5rem;
  letter-spacing: .1rem;
  font-weight: bold;
  padding: 1rem;
  transition: all 0.2s;
}

.form-label-group {
  position: relative;
  margin-bottom: 1rem;
}

.input {
  height: auto;
  border-radius: 2rem;
	margin: 2%;
}

.form-label-group>input,
.form-label-group>label {
  padding: var(--input-padding-y) var(--input-padding-x);
}

.form-label-group>label {
  position: absolute;
  top: 0;
  left: 0;
  display: block;
  width: 100%;
  margin-bottom: 0;
  line-height: 1.5;
  color: #495057;
  border: 1px solid transparent;
  border-radius: .25rem;
  transition: all .1s ease-in-out;
}

.form-label-group input::-webkit-input-placeholder {
  color: transparent;
}

.form-label-group input:-ms-input-placeholder {
  color: transparent;
}

.form-label-group input::-ms-input-placeholder {
  color: transparent;
}

.form-label-group input::-moz-placeholder {
  color: transparent;
}

.form-label-group input::placeholder {
  color: transparent;
}

.form-label-group input:not(:placeholder-shown) {
  padding-top: calc(var(--input-padding-y) + var(--input-padding-y) * (2 / 3));
  padding-bottom: calc(var(--input-padding-y) / 3);
}

.form-label-group input:not(:placeholder-shown)~label {
  padding-top: calc(var(--input-padding-y) / 3);
  padding-bottom: calc(var(--input-padding-y) / 3);
  font-size: 12px;
  color: #777;
}

.btn-google {
  color: white;
  background-color: #ea4335;
}

.btn-facebook {
  color: white;
  background-color: #3b5998;
}

	</style>
		
@stop

@section('pageIncludes')
    <script src="{{ url('public/assets/global/plugins/bower_components/jquery-validation/dist/jquery.validate.min.js') }}"></script>
@stop