@extends('layouts.loginLayout')
@section('content')
{{-- <body> --}}
  {{ Form::open([
      'url'=> '/auth/sign-up',
      'id' => 'regForm',
      'files' => true
    ])
  }}
    <div class= "tab">
      @foreach ($errors->all() as $error)
          <div class="alert alert-danger signup-alert" role="alert">
            {{ $error }}
          </div>
      @endforeach
      <h1>CREATE NEW ACCOUNT</h1>
      <hr>
      <br>
    <!--div class="card" style="padding-top:15px">
        <img src="{{ url('img/signup.png') }}" id="blah" alt="Upload your Profile Picture" class="user" >
        <p class="title"></p>
        <input type='file' name="member_photo" onchange="readURL(this);" accept="image/*"/>
      </div-->
      <br>
      
      <div class="form-inline form-sponsor">
        {{ Form::text('sponsor_name', '', [
            'class' => 'form-control',
            'placeholder' => 'Sponsor Name'
        ]) }}
      
        {{ Form::text('sponsor_membership_id', '', [
            'class' => 'form-control',
            'placeholder' => 'Sponsor Membership Id'
        ]) }}

      {{ Form::hidden('sponsor_id', '',[
        'class' => 'form-control',
        'placeholder' => 'Sponsor Id',
        'readonly' => 'readonly',
       
      ])}}

        <button class="btn btn-primary" id="validate-sponsor" data-toggle="modal" data-target="#signup-modal">
          Validate Sponsor
        </button>
      </div>
      <div class="row">
        <div class="form-group">
          <div class="col-md-4">
            {{ Form::text('first_name','',[
              'class' => 'form-control',
              'placeholder' => 'First Name'
              ]) }}
          </div>
          <div class="col-md-4" >
            {{ Form::text('middle_name','',[
                'class' => 'form-control',
                'placeholder' => 'Middle Name'
            ]) }}
          </div>  
          <div class="col-md-4" >
            {{ Form::text('last_name','',[
                'class' => 'form-control',
                'placeholder' => 'Last Name'
            ]) }}
          </div>
          <div>&nbsp;</div>
          <div class="col-md-4">
            {{ Form::text('account_id','',[
                'class' => 'form-control',
                'placeholder' => 'Account ID'
            ]) }}
          </div>
          <div class="col-md-4">
            
            {{ Form::text('activation_code','',[
                'class' => 'form-control',
                'placeholder' => 'Activation Code'
            ]) }}
          </div>
        </div>
      </div>
      </br>
      <div class="form-group">
        {{ Form::text('contact_no', '', [
            'class' => 'form-control',
            'placeholder' => 'Contact Number',
            'maxlength' => 11
        ]) }}
      </div>
      
      <div class="form-group">
        {{ Form::text('groupname', '', [
            'class' => 'form-control',
            'placeholder' => 'Group Name',
            'maxlength' => 11
        ]) }}
      </div>
      </div>
      <!--div class="custom-select" style="width:155px;">
         {{ Form::select('user_type', [
           'label' => 'User Type',
           'Passenger' => 'Passenger',
           'Driver' => 'Driver',
           'Both' => 'Both'
         ])}}
        </div-->
    </div>
    <br>
      <div class="form-group">
        {{ Form::email('email', '', [
            'class' => 'form-control',
            'placeholder' => 'Email Address'
        ]) }}
      </div>
      <div class="form-group"> 
        {{ Form::password('password', [
            'class' => 'form-control',
            'placeholder' => 'Password',
        ]) }}
      </div>    
      <div class="form-group"> 
        {{ Form::password('password_confirmation', [
            'class' => 'form-control',
            'placeholder' => 'Confirm Password',
        ]) }}
      </div>      
      {{-- <button type="button" class="btn btn-primary btn-lg btn-block" style="background: #0db14b;">SIGN UP</button> --}}
      {{ Form::submit('Sign Up', 
        ['class' => 'btn btn-primary btn-lg btn-block'])
      }}
    </div>             
    <hr class="my-4"/>
      {{-- <button class="btn btn-lg btn-google btn-block text-uppercase" type="submit"><i class="fab fa-google mr-2"></i> Sign up with Google</button>
      <button class="btn btn-lg btn-facebook btn-block text-uppercase" type="submit"><i class="fab fa-facebook-f mr-2"></i> Sign up with Facebook</button> --}}
    <br/>
    <p class="text-muted text-center sign-link" style="background-color: white; margin: 0; padding-bottom: 10px;">{{ Lang::get('labels.have_account') }} <a href="{{ url('auth/login#login') }}"> {{ Lang::get('labels.login') }}</a></p>
    {{ Form::close() }}

    
    <!-- Modal -->
    <div class="modal fade" id="signup-modal" tabindex="-1" role="dialog" aria-labelledby="signup-modal-label" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="signup-modal-label"></h5>

            {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button> --}}

          </div>
          <div class="modal-body" id="signup-modal-body">
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

{{-- </body> --}}

<style>
  .user{
  display: inline-block;
  width: 150px;
  height: 150px;

  object-fit: cover;
  }
#file{
  text-align: center;
  margin: auto;
}
* {
  box-sizing: border-box;
}
        
.btn-google {
  color: white;
  background-color: #ea4335;
}

.btn-facebook {
  color: white;
  background-color: #3b5998;
}

body {
  background-color: #074989;
  font-family: Arial;
}   


#regForm {
  background-color: #ffffff;
  margin: 100px auto;
  font-family: Raleway;
  padding: 40px;
  width: 70%;
  min-width: 300px;
  color:#0db14b;
}
h1 {
  text-align: center;  
  color:#074989;
}

input {
  padding: 10px;
  width: 100%;
  font-size: 17px;
  font-family:'Times New Roman', Times, serif;
  border: 1px solid #aaaaaa;
  color: #0db14b;
}

/* Mark input boxes that gets an error on validation: */
input.invalid {
  background-color: #ffdddd;
}

/* Hide all steps by default: */
.tab {
  display: none;
}
button {
  background-color: #4CAF50;
  color: #ffffff;
  border: none;
  padding: 10px 20px;
  font-size: 17px;
  font-family: Raleway;
  cursor: pointer;
}

button:hover {
  opacity: 0.8;
}


/* Make circles that indicate the steps of the form: */

.footer {
  
}

/* Mark the steps that are finished and valid: */
.form-group {
    color:black;
}

.form-sponsor{
  margin-bottom: 1em;
}


input{
  font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
  color: #000000 !important;
}

.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  max-width: 300px;
  margin: auto;
  text-align: center;
  font-family: arial;
}

.title {
  color: grey;
  font-size: 18px;
}

button {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  font-size: 18px;
}

a {
  text-decoration: none;
  font-size: 22px;
  color: black;
}

button:hover, a:hover {
  opacity: 0.7;
}
/* The container must be positioned relative: */
.custom-select {
  position: relative;
  font-family: Arial;
}

.custom-select select {
  display: none; /*hide original SELECT element: */
}

.select-selected {
  background-color:#00AFDE;
}

/* Style the arrow inside the select element: */
.select-selected:after {
  position: absolute;
  content: "";
  top: 14px;
  right: 10px;
  width: 0;
  height: 0;
  border: 6px solid transparent;
  border-color: #fff transparent transparent transparent;
}

/* Point the arrow upwards when the select box is open (active): */
.select-selected.select-arrow-active:after {
  border-color: transparent transparent #fff transparent;
  top: 7px;
}

/* style the items (options), including the selected item: */
.select-items div,.select-selected {
  color: #ffffff;
  padding: 8px 16px;
  border: 1px solid transparent;
  border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
  cursor: pointer;
}

/* Style items (options): */
.select-items {
  position: absolute;
  background-color: #00AFDE;
  top: 100%;
  left: 0;
  right: 0;
  z-index: 99;
}

/* Hide the items when the select box is closed: */
.select-hide {
  display: none;
}

.select-items div:hover, .same-as-selected {
  background-color: rgba(0, 0, 0, 0.1);
}
</style>

<script>
var x, i, j, selElmnt, a, b, c;
/* Look for any elements with the class "custom-select": */
x = document.getElementsByClassName("custom-select");
for (i = 0; i < x.length; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  /* For each element, create a new DIV that will act as the selected item: */
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /* For each element, create a new DIV that will contain the option list: */
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < selElmnt.length; j++) {
    /* For each option in the original select element,
    create a new DIV that will act as an option item: */
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /* When an item is clicked, update the original select box,
        and the selected item: */
        var y, i, k, s, h;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        h = this.parentNode.previousSibling;
        for (i = 0; i < s.length; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            for (k = 0; k < y.length; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
    /* When the select box is clicked, close any other select boxes,
    and open/close the current select box: */
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
}

function closeAllSelect(elmnt) {
  /* A function that will close all select boxes in the document,
  except the current select box: */
  var x, y, i, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  for (i = 0; i < y.length; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < x.length; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}

/* If the user clicks anywhere outside the select box,
then close all select boxes: */
document.addEventListener("click", closeAllSelect);
  
$(document).ready(function(){
  

  // $('#signup-modal'));

  $('#validate-sponsor').click(function(e){

    // signup_modal.modal('show');


    e.preventDefault();

    // modal

    let signup_modal = $('#signup-modal');
    let modal_label = signup_modal.find('#signup-modal-label');
    let modal_body = signup_modal.find('#signup-modal-body');

    let sponsor = $('input[name="sponsor_name"]');
    let sponsor_id = $('input[name="sponsor_id"]');
    let sponsor_membership_id = $('input[name="sponsor_membership_id"]');

    let sponsor_name = $('input[name="sponsor_name"]').val();
    let sponsor_members_id = sponsor_membership_id.val();


    console.log(sponsor_name);

    $.ajax({
      url: '/auth/validate-sponsor',
      method: 'GET',
      dataType: 'JSON',
      data: {
        sponsor_name : sponsor_name,
        sponsor_membership_id : sponsor_members_id
      }
    }).done(function(data){

      console.log(data);

      if(data.status == 'success'){
 
          modal_label.text('Success');
          modal_body.text('Found : ' + data.name);
          
          sponsor.val(data.name);
          sponsor_id.val(data.user_id);
          sponsor_membership_id.val(data.membership_id);
        

      }
      else if(data.status == 'error'){

        modal_label.text('Error');
        modal_body.text('Unable to find an Sponsor with a name/id ' + sponsor_name);

        sponsor.val('');
        sponsor_id.val('');

      }


    }).fail(function(data){
      console.log(data);
    });


  });


    $('#signup-modal').on('hidden.bs.modal', function(e){

      let label = $(this).find('#signup-modal-label');
      let body = $(this).find('#signup-modal-body');

      label.empty();

      body.empty();

    }); 

}); 

    var currentTab = 0; 
    showTab(currentTab); 
    
    function showTab(n) {
      // This function will display the specified tab of the form...
      var x = document.getElementsByClassName("tab");
      x[n].style.display = "block";
      //... and fix the Previous/Next buttons:
      // if (n == 0) {
      //   // document.getElementById("prevBtn").style.display = "none";
      // } else {
      //   document.getElementById("prevBtn").style.display = "inline";
      // }
      // if (n == (x.length - 1)) {
      //   document.getElementById("nextBtn").innerHTML = "Submit";
      // } else {
      //   document.getElementById("nextBtn").innerHTML = "Next";
      // }
      //... and run a function that will display the correct step indicator:
      // fixStepIndicator(n)
    }
    
    // function nextPrev(n) {
    //   // This function will figure out which tab to display
    //   var x = document.getElementsByClassName("tab");
    //   // Exit the function if any field in the current tab is invalid:
    //   if (n == 1 && !validateForm()) return false;
    //   // Hide the current tab:
    //   x[currentTab].style.display = "none";
    //   // Increase or decrease the current tab by 1:
    //   currentTab = currentTab + n;
    //   // if you have reached the end of the form...
    //   if (currentTab >= x.length) {
    //     // ... the form gets submitted:
    //     document.getElementById("regForm").submit();
    //     return false;
    //   }
    //   // Otherwise, display the correct tab:
    //   showTab(currentTab);
    // }
    
    // function validateForm() {
    //   // This function deals with validation of the form fields
    //   var x, y, i, valid = true;
    //   x = document.getElementsByClassName("tab");
    //   y = x[currentTab].getElementsByTagName("input");
    //   // A loop that checks every input field in the current tab:
    //   for (i = 0; i < y.length; i++) {
    //     // If a field is empty...
    //     if (y[i].value == "") {
    //       // add an "invalid" class to the field:
    //       y[i].className += " invalid";

    //       valid = false;
    //     }
    //   }
    
    //   if (valid) {
    //     document.getElementsByClassName("step")[currentTab].className += " finish";
    //   }
    //   return valid; 
    // }
    
    // function fixStepIndicator(n) {

    //   var i, x = document.getElementsByClassName("step");
    //   for (i = 0; i < x.length; i++) {
    //     x[i].className = x[i].className.replace(" active", "");
    //   }

    //   x[n].className += " active";
    // }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

</script>
     


  
@stop