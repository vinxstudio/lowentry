<?php namespace App\Http\Modules\FrontEnd\Controllers;

use App\Helpers\MailHelper;
use App\Helpers\ShortEncrypt;
use App\Http\AbstractHandlers\MainAbstract;
use App\Http\Modules\FrontEnd\Validation\FrontEndValidationHandler;
use App\Http\TraitLayer\BinaryTrait;
use App\Models\Accounts;
use App\Models\ActivationCodes;
use App\Models\Details;
use App\Models\User;
use App\Models\Withdrawals;
use App\Models\Membership;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Binary;
use App\Models\Company;
use App\Models\Logs;
use App\Models\CompanyEarnings;
use App\Models\UserDetails;
use App\Models\UserNetwork;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class FrontEndController extends MainAbstract
{

    use BinaryTrait;

    protected $viewPath = 'FrontEnd.views.';

    function getLogin()
    {


        // Mail::send('emails.mail', [], function ($message) {
        //     $message->from('waifu.hifumi@gmail.com')->to('waifu.hifumi@gmail.com', 'John Smith')->subject('Welcome!');
        // });

        // $data = [];
        // $data['to'] = 'waifu.hifumi@gmail.com';
        // $data['name'] = 'Hifumi My Love';
        // $data['subject'] = 'Testing Email My Love';

        // mail_sender($data);

        // return "Yeah Boy";

        return view($this->viewPath . 'login');
    }
    function encodeCodes(Request $request)
    {

        // $batch = [
        //     'name' => $theBatchName,
        //     'type' => $codeType
        // ];

        // $manageModel = new ModelHelper();
        // $batchObject = $manageModel->manageModelData(new ActivationCodeBatches, $batch);

        // $codes = new ActivationCodeHelperClass();
        // $codes
        //     ->setBatchID($batchObject->id)
        //     ->setCodeType($inputs['type'])
        //     ->setNumOfZeros(5)
        //     ->setPatternEveryLetter(3)
        //     ->setPrefixLength(5);

        // $theCodes = $codes->generateCodes($inputs['quantity'], $inputs['type']);

        // ActivationCodes::insert($theCodes);
    }
    function validateUsername(Request $request)
    {
        $thisusername = $request->username;
        /*
		User::find(0);
		return view( $this->viewPath . 'sign_up' )
            ->with([
                'id'=>0,
                'user'=>User::find(0),
				'membership'=>Membership::paginate(50),
                'referral'=>$request->ref,
				'úpline'=>$uplineid,
				'sponsor'=>$sponsorid,
				'node'=>$node,
                'suffix'=>($request->ref != null) ? '?ref=' . $request->ref : null
            ]);
		 */
        $usercount = User::where('username', $thisusername)->count();

        if ($usercount > 0) {
            $thisuser = User::where('username', $thisusername)->get();
            $detailsId = $thisuser->user_details_id;

            $userDetails = Details::find($detailsId);
            $result = $userDetails->first_name . " " . $userDetails->middle_name . " " . $userDetails->last_name;
            //$result->error = true;
        } else {
            $result = "Does not exist";
        }

        return $result;
        return view($this->viewPath . 'auth/validateusername')->with([result => $result]);
    }

    /**
	 * Old postLogin
	 */

    // function postLogin(Request $request)
    // {

    // 	$validate = new FrontEndValidationHandler();
    // 	$validate
    // 		->setInputs($request->input());

    // 	$result = $validate->validate();

    // 	Session::flash($result->message_type, $result->message);

    // 	if (!$result->error) {
    // 		$role = Auth::user()->role;
    // 	}

    // 	$nextUrl = null;
    // 	if (!$result->error) {
    // 		$user = Auth::user();
    // 		$user->details->email;
    // 		if ($user->details->email != null and isEmailRequired() and $user->role == MEMBER_ROLE) {
    // 			$code = ShortEncrypt::make(time());
    // 			$user->verification_code = $code;
    // 			$user->save();
    // 			$nextUrl = 'auth/verify';
    // 			$mail = new MailHelper();
    // 			$mail->setUserObject($user)->setVerificationCode($code);
    // 			$mail->sendMail(LOGIN_KEY);
    // 		} else {
    // 			if ($role == 'teller') {
    // 				$nextUrl = sprintf('%s/activation-codes', $role);
    // 			} else {
    // 				$nextUrl = sprintf('%s/dashboard', $role);
    // 			}
    // 		}
    // 	}

    // 	return ($result->error) ? redirect('auth/login') : redirect($nextUrl);

    // }

    function getSignUp(Request $request)
    {

        $uplineid = $request->input('uplineid');
        $sponsorid = $request->input('sponsorid');
        $node = $request->input('node');

        $activationCode = new ActivationCodes();

        $upline_info = $activationCode->get_account_name($uplineid);
        $sponsor_info = $activationCode->get_account_name($sponsorid);

        return view($this->viewPath . 'sign_up')
            ->with([
                'id' => 0,
                'user' => User::find(0),
                'membership' => Membership::paginate(50),
                'referral' => $request->ref,
                'upline_name' => (!empty($upline_info)) ? $upline_info->first_name . ' ' . $upline_info->last_name : '',
                'upline' => $uplineid,
                'sponsor_name' => (!empty($sponsor_info)) ? $sponsor_info->first_name . ' ' . $sponsor_info->last_name : '',
                'sponsor' => $sponsorid,
                'node' => $node,
                'suffix' => ($request->ref != null) ? '?ref=' . $request->ref : null
            ]);
    }

    /**
	 * Old Sign Up Function
	 * 
	 * Commented on February 15, 1:37 PM
	 */

    // function postSignUp(Request $request)
    // {

    // 	$result = new \stdClass();
    // 	$result->error = false;
    // 	$result->message = '';

    // 	if ($request->input('own_Account') && ($request->input('own_Account') == 'Own Account')) {

    // 		// $details = new Details();
    // 		$thisuser = Auth::user();
    // 		$request->first_name = $thisuser->details->first_name;
    // 		$request->last_name = $thisuser->details->last_name;
    // 		$request->email = $thisuser->details->email;
    //          //   $details->save();

    //         //    $user = new User();
    //        // $request->username = $thisuser->usernname.'-'.
    //         //    $user->password = Hash::make($request->password);
    //         //    $user->user_details_id = $details->id;
    //         //    $user->paid = ($request->have_activation == YES_STATUS) ? TRUE_STATUS : FALSE_STATUS;
    // 		$thisusername = $request->input('forusername');
    // 		$usercount = User::where("group_id", $thisuser->group_id)->count();
    // 		// die('total = '.$usercount.' - '.$thisusername);
    // 		if ($usercount > 7) {
    // 			$result->message = "Own User Account limit exceeded";
    // 			$result->error = true;
    // 		} else {
    // 			$result->error = false;
    // 		}
    // 		//$validation = Validator::make($request->input());
    // 	} else {

    // 		$additional_rules = [
    // 			'bank_name' => 'required',
    // 			'bank_account_name' => 'required',
    // 			'bank_account_number' => 'required',
    // 			'password' => 'required|min:8',
    // 			'password_confirm' => 'same:password|required',
    // 			'first_name' => 'required',
    // 			'last_name' => 'required',
    // 			'username' => 'required|unique:users'
    // 		];

    // 		if ($request->have_activation == YES_STATUS) {
    // 			$additional_rules['upline_id'] = 'required';
    // 			$additional_rules['account_id'] = 'required';
    // 			$additional_rules['activation_code'] = 'required';
    // 		}

    // 		if (isEmailRequired()) {
    // 			$additional_rules['email'] = 'required|email|unique:user_details,email';
    // 		}

    // 		$validation = Validator::make($request->input(), $additional_rules);

    // 		if ($validation->fails()) {
    // 			$result->error = true;
    // 		}
    // 	}
    // 	if ($request->have_activation == YES_STATUS) {
    // 		$searchUpline = ActivationCodes::where([
    // 			'account_id' => $request->upline_id,
    // 			'status' => 'used'
    // 		])->get();

    // 		$searchSponsor = ActivationCodes::where([
    // 			'account_id' => $request->sponsor_id,
    // 			'status' => 'used'
    // 		])->get();

    // 		$checkActivation = ActivationCodes::where([
    // 			'account_id' => $request->account_id,
    // 			'code' => $request->activation_code,
    // 			'status' => 'available'
    // 		])->get();

    // 		$account = (!$searchUpline->isEmpty()) ? Accounts::where('code_id', $searchUpline->first()->id)->get()->first() : null;

    // 		$sponsorAccountId = (!$searchSponsor->isEmpty()) ? Accounts::where('code_id', $searchSponsor->first()->id)->get()->first() : null;

    // 		$uplineDownlineCount = (!$searchUpline->isEmpty()) ? Accounts::where('upline_id', $account->id)->count() : 0;

    // 			/*if($request->input('own_Account') && ($request->input('own_Account') == 'Own Account')) {
    // 				$thisusername = $request->input('forusername');
    // 				$usercount = User::where('username','LIKE',"{$thisusername}%")->count();
    // 				if($usercount >= 31) {
    // 					$result->message = "Own User Account limit exceeded";
    // 					$result->error = true;
    // 				}
    // 			}
    // 		 */

    // 		if (isset($validation) && $validation->fails()) {
    // 			$result->error = true;
    // 		} else if ($checkActivation->isEmpty()) {

    // 			$result->message = Lang::get('messages.invalid_activation_code');
    // 			$result->error = true;

    // 		} else if ($searchUpline->isEmpty()) {

    // 			$result->error = true;
    // 			$result->message = Lang::get('messages.invalid_upline');

    // 		} else if ($uplineDownlineCount >= 2) {

    // 			$result->error = true;
    // 			$result->message = Lang::get('messages.upline_has_complete_nodes');

    // 		} else if (Accounts::where('node', $request->node_placement)->where('upline_id', $account->id)->count() > 0) {

    // 			$result->message = Lang::get('messages.node_already_occupied');
    // 			$result->error = true;

    // 		} else if (!isset($account->id)) {
    // 			$result->message = Lang::get('There was a problem on encoding. Please try again!');
    // 			$result->error = true;
    // 		}
    // 	} else {

    // 		$member_type = $request->type;
    // 		$type = true;
    // 	}

    // 	if (!$result->error) {
    // 		try {



    // 			$details = new Details();
    // 			$details->first_name = $request->first_name;
    // 			$details->last_name = $request->last_name;
    // 			$details->email = $request->email;
    // 			$details->save();

    // 			$logs = new Logs();
    // 			$logs->logs_name = 'Enter Details of ' . $request->first_name . ' ' . $request->last_name;
    // 			$logs->logs_description = 'User Details has been inserted....';
    // 			$logs->save();

    // 			if ($request->input('own_Account') && ($request->input('own_Account') == 'Own Account')) {
    // 				$user = new User();
    // 				$user->username = $request->input('forusername') . "-" . $details->id;

    // 				$user->password = $thisuser->password;
    // 				$user->user_details_id = $details->id;
    // 				$user->paid = true;
    // 				$user->member_type_id = $checkActivation->first()->type_id;
    // 				$user->role = MEMBER_ROLE;
    // 				$user->group_id = $thisuser->group_id;
    // 				$user->save();
    // 			} else {
    // 				$user = new User();
    // 				$user->username = $request->username;
    // 				$user->password = Hash::make($request->password);
    // 				$user->user_details_id = $details->id;
    // 				$user->paid = ($request->have_activation == YES_STATUS) ? TRUE_STATUS : FALSE_STATUS;
    // 				//$user->paid = 'true';
    // 				//addded 
    // 				if (isset($type)) {
    // 					$user->member_type_id = $member_type;
    // 				} else {
    // 					$activationCode = $request->activation_code;

    // 					//$user->paid = 'true';
    // 					//$user->needs_activaton = 'true';
    // 					$user->member_type_id = $checkActivation->first()->type_id;
    // 				}

    // 				// end added

    // 				$user->role = MEMBER_ROLE;
    // 				$user->save();

    // 				$new_users = User::find($user->id);
    // 				$new_users->group_id = $new_users->id;
    // 				$new_users->update();
    // 			}
    //            // $this->saveDetails($user->id, $request);

    // 			if ($request->have_activation == YES_STATUS) {

    // 				$setaccount = new Accounts();
    // 				$setaccount->user_id = $user->id;
    // 				$setaccount->code_id = $checkActivation->first()->id;
    // 				//if(isset($account->id)) {
    // 				$setaccount->upline_id = $account->id;
    // 				//} else {

    // 				//}
    // 				$setaccount->sponsor_id = $sponsorAccountId->id;
    // 				$setaccount->node = $request->node_placement;

    // 				$setaccount->save();

    // 				$setActivationCode = ActivationCodes::find($checkActivation->first()->id);
    // 				$setActivationCode->status = 'used';
    // 				$setActivationCode->user_id = $user->id;
    // 				$setActivationCode->paid_by_balance = 'false';
    // 				$setActivationCode->save();

    // 				$logs = new Logs();
    // 				$logs->logs_name = 'Enter Accounts of ' . $user->id;
    // 				$logs->logs_description = 'Activation Codes has been inserted....';
    // 				$logs->save();

    // 				$binary = new Binary();
    // 				$membership = Membership::find($user->member_type_id);
    // 				$companyIncome = $membership->entry_fee - ($membership->referral_income + $membership->money_pot);

    //                 // $sponsor_id = ($this->sponsor_id > 0) ? $this->sponsor_id : $request->sponsor;

    // 				//$activationCode = ActivationCodes::find($activation_id);
    // 				//$company = Company::find($this->companyID);

    // 				$sponsor_id = $sponsorAccountId->id;
    // 				$sponsor = ($sponsor_id > 0) ? Accounts::find($sponsorAccountId->id) : null;

    // 				//$sponsor_id = (isset($sponsor->id) and $sponsor->id > 0) ? $sponsor->id : 0;


    // 				$binary->runReferral($companyIncome, $sponsor_id, $sponsor, $setActivationCode, $user->id);

    // 				$logs = new Logs();
    // 				$logs->logs_name = 'Enter Referral of ' . $sponsor_id;
    // 				$logs->logs_description = 'Direct Referral has been inserted....';
    // 				$logs->save();

    // 				$binary->runPointsValue($setaccount->upline_id, $user->id, $request->node_placement, $membership->points_value);

    // 				$logs = new Logs();
    // 				$logs->logs_name = 'Enter Points Value ' . $sponsor_id;
    // 				$logs->logs_description = 'Points Value has been inserted....';
    // 				$logs->save();

    // 				$pairing = $binary
    // 					->setMemberObject($setaccount)
    // 					->crawl();

    // 				$logs = new Logs();
    // 				$logs->logs_name = 'Enter Pairing of ' . $user->id;
    // 				$logs->logs_description = 'Craw to binary has been inserted....';
    // 				$logs->save();

    // 				 //$companyIncome = $this->calculateCompanyBaseIncome();
    // 				 //private function calculateCompanyBaseIncome(){
    // 				//	$result = new \stdClass();
    // 				//	$company = Company::find($this->companyID);

    // 				//	$result->referral_income = $company->referral_income;
    // 				//	$result->money_pot = $company->money_pot;
    // 					//$result->company_income = $company->entry_fee - ($company->referral_income + $company->money_pot);
    // 				//	return $result;
    // 				//}



    // 				/*
    //                 $result = $this
    //                     ->setUserObject($user)
    //                     ->setSponsorId($sponsorAccountId->id)
    //                     ->setActivationCodeId($checkActivation->first()->id)
    //                     ->setUplineId($account->id)
    //                     ->validateAdminRegistration($request);
    // 				 */

    // 			}

    // 			$mail = new MailHelper();
    // 			$mail->setUserObject($user);
    // 			$mail->sendMail(REGISTRATION_KEY);

    // 			if (!isset(Auth::user()->id)) {
    //                 //if no one is logged in, then auto sign in
    // 				Auth::loginUsingId($user->id);
    // 				return redirect('member/dashboard');
    // 			}

    // 		} catch (\Exception $e) {
    // 			$result->error = true;
    // 			$result->message = $this->formatException($e);
    // 		}
    // 	}

    // 	$suffix = ($request->ref != null) ? '?ref=' . $request->ref : null;
    // 	if (isset($validation)) {
    // 		$validationErrors = $validation->errors();
    // 	} else {
    // 		$validationErrors = "";
    // 	}
    // 	$suffix .= '?node=' . $request->node_placement;
    // 	return ($result->error) ? redirect(sprintf('auth/sign-up%s', $suffix))
    // 		->withInput()
    // 		->withErrors($validationErrors)
    // 		->with('danger', $result->message) : redirect('auth/login')
    // 		->with('success', $result->message);

    // }

    function getVerify()
    {
        if (!Auth::check()) {
            return redirect('auth/login');
        }

        return view($this->viewPath . 'verify');
    }

    function postVerify(Request $request)
    {
        $user = Auth::user();

        if ($request->verification_code != $user->verification_code) {
            return back()->with('danger', Lang::get('messages.invalid_code'));
        } else {
            $user->verification_code = '';
            $user->save();
            return redirect('member/dashboard');
        }
    }

    function getTestMail()
    {
        $mail = new MailHelper();
        $mail->setPreview(true)->setUserObject(User::find(2))->setWithdrawalObject(Withdrawals::find(1));
        return $mail->sendMail(WITHDRAWAL_KEY);
    }

    function getWide()
    {
        return view($this->viewPath . 'wide');
    }

    function getAwarding()
    {
        return view($this->viewPath . 'awarding');
    }

    function getRegistrationSocials()
    {
        return view($this->viewPath . 'registration-socials');
    }

    /**  System
	 * 	
	 * Functions
	 * - validate-sponsor
	 * -- Validate sponsor if it exists
	 * - sign-up
	 * -- new function for registering new member
	 * 
	 */

    /**
	 * ValidateSponsor
	 */

    function getValidateSponsor(Request $request)
    {
        $names = $request->sponsor_name;
        $sponsor_member_id = $request->sponsor_membership_id;

        $data = [];
        $data['status'] = 'error';
        $code = 200;

        $user_details = null;
        $full_name = '';

        if(!empty($names)){

            $names = explode(" ", $names);
            
            $key = array_search("", $names);
            
            while ($key) {
                
                unset($names[$key]);
                
                $key = array_search("", $names);
            }
            
            if (!$request->sponsor_name) {
                return response()->json($data, $code);
            }
            
            foreach ($names as $name) {
                
                $user_details = UserDetails::select('id', 'first_name', 'middle_name', 'last_name')
                ->where('first_name', 'LIKE', '%' . $name . '%')
                ->orWhere('middle_name', 'LIKE', '%' . $name . '%')
                ->orWhere('last_name', 'LIKE', '%' . $name . '%')
                ->first();
            }
            
            $user_id = -1;

            $data['type'] = 'name';
            
    
        }
        else if(!empty($sponsor_member_id)){

            $user = User::where('membership_id', $sponsor_member_id)->first();

            $user_details = UserDetails::where('id', $user->user_details_id)->first();

            $data['type'] = 'membership_id';

        }


        if ($user_details) {
                
            if (isset($user_details->first_name))
            $full_name .= $user_details->first_name . ' ';
            
            if (isset($user_details->middle_name))
            $full_name .= $user_details->middle_name . ' ';
            
            if (isset($user_details->last_name))
            $full_name .= $user_details->last_name . ' ';
            
            $user_id = User::where('user_details_id', '=', $user_details->id)->first();
            
            if ($user_id) {
                
                $data['status'] = 'success';
                $data['name'] = $full_name;
                $data['user_id'] = $user_id->id;
                $data['membership_id'] = $user_id->membership_id;
            }
        }

        return response()->json($data, $code);
    }


    /**
	 * SignUp
	 */

    function postSignUp(Request $request)
    {
        
        $first_name = $request->first_name;
        $middle_name = $request->middle_name;
        $last_name = $request->last_name;
        $email = $request->email;
        $password = $request->password;
        $user_type =$request->user_type;
        $sponsor_id = $request->sponsor_id;

        $groupname = $request->groupname;

        $message = [
            'first_name.required' => 'First name is required',
            'last_name.required' => 'Last name is required',
            'email.required' => 'Email is required',
            'email.unique' => 'Email already exists',
            'email.email' => 'Must be a valid email',
            'password.required' => 'Password is required',
            'password.min' => 'Minimum password of 6 characters',
            'password.max' => 'Maximum password of 18 characters',
            'password.confirmed' => 'Confirmation Password not equal',
            'password_confirmation.required' => 'Confirmation Password is required',
            'contact_no.required' => 'Contact Number is required',
            'contact_no.numeric' => 'Contact number must be a number',
            #'contact_no.min' => 'Contact number must be minimun of 11 numbers',
            #'contact_no.max' => 'Contact number must be maximun of 11 numbers',
            'sponsor_name.required' => 'Sponsor name is required',
            'sponsor_id.required' => 'Sponsor must be validated and existed',
            'member_photo.required' => 'Image is required',
            'member_photo.max' => 'Maximum Size of Image is 2 kilobytes',
            'activation_code.required' => 'Activation Code is required',
            'activation_id.required' => 'Activation ID is required'
            //'user_type.required' => 'User Type is required'
            
        ];

        $this->validate($request, [
            'first_name' => 'required|max:75',
            'middle_name' => 'max:70',
            'last_name' => 'required|max:70',
            'email' => 'required|email|unique:users,email|max:75',
            'password' => 'required|confirmed|min:6|max:18',
            'password_confirmation' => 'required',
            'contact_no' => 'required|numeric',
            'sponsor_name' => 'required',
            'sponsor_id' => 'required',
            'activation_code' => 'required',
            'account_id' => 'required'
        ], $message);

        $checkActivation = ActivationCodes::where([
            'account_id'=>$request->account_id,
            'code'=>$request->activation_code,
            'status'=>'available'
        ])->get();


        $sponsorAccountId = Accounts::where('user_id', $request->sponsor_id)->get();
       
        if ($checkActivation->isEmpty()){

           // $message = Lang::get('messages.invalid_activation_code');
           // $error = true;
           return redirect()->back()->withErrors('Invalid Activation Code and Activation ID');

        } else {
        
            DB::beginTransaction();
            try {

                $image = null;

                //if ($request->hasFile('member_photo')) {

                 //   $filename = rand(10, 20);

                //    $file_extension = $request->file('member_photo')->getClientOriginalExtension();

                //    $image = $request->file('member_photo')->move('public/member_photo/', $request->file('member_photo') . '.' . $file_extension);
               // }

                $user_details = new UserDetails();

                $user_details->first_name = $first_name;
                $user_details->middle_name = $middle_name;
                $user_details->last_name = $last_name;
               // $user_details->photo = $image;
            // $user_details->user_type = $user_type;

                $user_details->save();


                $user = new User();

                $user->username = $last_name . rand(1000, 10000);
                $user->email = $email;
                $user->password = Hash::make($password);
                $user->user_details_id = $user_details->id;
                $user->member_type_id = $checkActivation->first()->type_id;
                $user->groupname = $groupname;

                $user->save();

                $user->group_id = $user->id;

                $members_id = Carbon::now()->format('Y');
                $members_id .= '-';
                $string_id = (string)$user->id;
                $length = strlen($string_id);
                $mems = 8 - $length;
                for ($i = 0; $i < $mems; $i++) {
                    $members_id .= 0;
                }
                $members_id .= $user->id;
                $user->membership_id = $members_id;
                $user->save();

                $user_network = new UserNetwork();

                $user_network->user_id_owner = $user->id;
                $user_network->user_id_sponsor = $sponsor_id;


                //$user_network->status = 'Admin Approved';
                $user_network->status = UserNetwork::STATUS_ADMIN_APPROVED;
                

                $user_network->save();

                $data = [];

                $sponsor_user = User::find($sponsor_id);

                $setActivationCode = ActivationCodes::find($checkActivation->first()->id);
    				$setActivationCode->status = 'used';
     				$setActivationCode->user_id = $user->id;
                     $setActivationCode->paid_by_balance = 'false';
                    
                     $setActivationCode->save();
                 
                //insert on accounts
                $setaccount = new Accounts();
                     $setaccount->user_id = $user->id;
                     $setaccount->code_id = $checkActivation->first()->id;
                    // $sponsorAccountId
                     $setaccount->upline_id = $sponsorAccountId->first()->id;
                     $setaccount->sponsor_id = $sponsorAccountId->first()->id;
                 
                     $setaccount->save();

                //insert direct referral
                $binary = new Binary();
					$membership = Membership::find($user->member_type_id);
					$companyIncome = $membership->entry_fee - ($membership->referral_income + $membership->money_pot);
					$sponsor_id = $sponsorAccountId->first()->id;
					$sponsor = ($sponsor_id > 0) ? Accounts::find($sponsorAccountId->first()->id) : null;

                    $binary->runReferral( $companyIncome, $sponsor_id, $sponsor, $setActivationCode, $user->id);
					
					$logs = new Logs();
					$logs->logs_name = 'Enter Referral of '.$sponsor_id;
					$logs->logs_description = 'Direct Referral has been inserted....';
                    $logs->save();
                    
                    $binary->runIndirectReferral($companyIncome, $sponsor_id, $sponsor, $setActivationCode, $user->id);
					
					$logs = new Logs();
					$logs->logs_name = 'Enter Indirect Referral of '.$sponsor_id;
					$logs->logs_description = 'Indirect Referral has been inserted....';
					$logs->save();

                DB::commit();
            } catch (\Exception $ex) {

                $error_message = 'Something went wrong! '. $ex;

                Log::debug('Sign Up');
                Log::debug('Date : ' . Carbon::now());
                Log::debug('Error : ' . $ex);

                return redirect()->back()->withErrors($error_message);

                DB::rollback();
            }
        


            /*
            try {
                if (isset($sponsor_user->email)) {
                    $data['to'] = $sponsor_user->email;
                    $data['name'] = $first_name . ' ' . $middle_name . ' ' . $last_name;
                    $data['subject'] = 'New Member';

                    mail_sender($data);
                }
            } catch (\Exception $ex) { }
            */

            $message = 'You have successfully Registered you can now login';

            return redirect('auth/login')->with([
                'signup_message' => $message
            ]);
        }
    }

    /**
	 * Login Function
	 */

    function postLogin(Request $request)
    {

        $email = $request->email;
        $password = $request->password;

        $message = [];

        $this->validate($request, [], $message);

        $remember = ($request->remember_me && $request->remember_me == 'on') ? true : false;

        $credentials = [
            'email' => $email,
            'password' => $password
        ];

        if (auth()->attempt($credentials, $remember)) {


            $role = auth()->user()->role;

            session('role', $role);

            if ($role == 'member')
                return redirect('/member/dashboard/');
            else if ($role == 'teller')
                return redirect('/teller/activation-codes');
            else if ($role == 'admin')
                return redirect('/admin/dashboard');
            else if ($role == 'accounting')
                return redirect('/admin/dashboard');
        }

        $message = 'Invalid Username or Password';

        return redirect()->back()->withErrors($message);
    }
}
