
@extends('layouts.master')
@section('content')
<div class="div">
        {{-- <a class="btn btn-primary pull-left" href="{{ url('admin/members/form') }}"><i class="fa fa-plus"></i> {{ Lang::get('labels.new') }}</a> --}}
        <div class="clearfix"></div>
        <br/>
        <div class="pull-right">
            {{ Form::open(['class'=>'form-horizontal form-bordered']) }}
                <div class='col-md-3'>
                    <div class="form-group">
                        <div class='input-group date'>
                            <span class="input-group-addon">
                                <i class="fas fa-search"></i>
                            </span>
                            <input type='text' name="search_keyword" class="form-control" value=""/>
                        </div>
                    </div>
                </div>
                <div class='col-md-3'>
                    <div class="form-group">
                        <div class='input-group date'>
                            <input type='button' class="form-control btn btn-primary" style="background-color:#019541;" value="SEARCH"/>
                        </div>
                    </div>
                </div>
        </div>
    
            {{ Form::close() }}
    
              

    <table class="1dataTable table table-bordered table-hover table-striped">
        <thead>
            <th>ID</th>
            <th>Customer Id</th>
            <th>Customer</th>
            <th>Driver Id</th>
            <th>Driver</th>
            <th>Car</th>
            <th>Service Type</th>
            <th>Customer Name</th>
            <th>Destination</th>
            <th>Destination Lat</th>
            <th>Destination Lang</th>
            <th>Distance </th>
            <th>Driver Name</th>
            <th>Pick Up</th>
            <th>Pick Up Lat</th>
            <th>Pick Up Lang</th>
            <th>Price</th>
            <th>Final Price</th>
            <th>Adjust Price Type</th>
            <th>Adjust Price Percent</th>
            <th>Adjust Price Amount</th>
            <th>Adjust Price Details</th>
            <th>Lang</th>
            <th>Lat</th>
            <th>Time Book</th>
            <th>Time Pickup</th>
            <th>Time Arrival</th>
            <th>Created At</th>
            <th>Updated At</th>
       
        </thead>
        <tbody style="font-size: 80%">
            @foreach($trip_transactions as $trip)
        
            <tr>
                    <td class="hidden-xs text-center col-md-1">
                       {{$trip->id}}
                    </td>
               
              
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->customer_id}}
                    </td>
                    
                    <td class="hidden-xs text-center col-md-1">
                           {{$trip->customer}}
                        </td>
                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->driver_id}}
                        </td>
                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->driver}}
                        </td>
                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->car}}
                        </td>
                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->service_type}}
                        </td>
                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->customer_name}}
                        </td>
                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->destination}}
                        </td>
                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->destination_lat}}
                        </td>
                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->destination_lng}}
                        </td>
                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->distance}}
                        </td>
                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->driver_name}}
                        </td>
                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->pickup}}
                        </td>
                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->pickup_lat}}
                        </td>
                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->pickup_lng}}
                        </td>
                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->price}}
                        </td>
                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->final_price}}
                        </td>
                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->adjust_price_type}}
                        </td>
                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->adjust_price_percent}}
                        </td>
                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->adjust_price_amount}}
                        </td>
                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->adjust_price_details}}
                        </td>
                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->lng}}
                        </td>
                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->lat}}
                        </td>

                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->time_book}}
                        </td>
                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->time_pickup}}
                        </td>
                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->time_arrival}}
                        </td>
                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->created_at}}
                        </td>
                        
                    <td class="hidden-xs text-center col-md-1">
                            {{$trip->updated_at}}
                        </td>
          
            </tr>       
            @endforeach 
@stop

<style>
    .div{
        padding-left: 200px;
    }

    </style>