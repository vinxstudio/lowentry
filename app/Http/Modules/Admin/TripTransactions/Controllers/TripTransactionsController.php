<?php 

namespace App\Http\Modules\Admin\TripTransactions\Controllers;

use App\Http\AbstractHandlers\AdminAbstract;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class TripTransactionsController extends AdminAbstract {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	protected $viewpath = 'Admin.TripTransactions.views.';

	public function getIndex()
	{


		$trip_transactions = DB::connection('mysql2')->select("SELECT * FROM trip_transaction");
		// return $trip_transactions;
		// $conn = new PDO('mysql: host=taxiapi.iamcebu.com;dbname=taxiapi;port=3306','taxi_api_read_user','lOpA4G6fydRCWToS');

		#$trip_transactions = TripTransactions::all()->toArray();
		return view($this->viewpath . 'index')->with(['trip_transactions' => $trip_transactions]);
	}


}
