@extends('layouts.master')
@section('content')

    <div class="main">
        @include('Admin.Connect.views.form')
    </div>

    <div class="main">
        <table class="table table-bordered table-hover table-striped" style="background:#019541;">
            <thead>
                <th>{{ Lang::get('connection.url') }}</th>
                <th>{{ Lang::get('connection.passcode') }}</th>
                <th>{{ Lang::get('labels.action') }}</th>
            </thead>
            <tbody>
                @if ($connections->isEmpty())
                    <tr>
                        <td colspan="3">
                            <center>{{ Lang::get('connection.empty') }}</center>
                        </td>
                    </tr>
                @else
                    @foreach ($connections as $row)
                        <tr>
                            <td><a href="{{ $row->url }}" target="_blank">{{ $row->url }}</a></td>
                            <td>{{ $row->passcode }}</td>
                            <td>
                                <a class="btn btn-danger btn-xs" href="{{ url('admin/connections/delete/'.$row->id) }}"><i class="fa fa-trash"></i> {{ Lang::get('labels.delete') }}</a>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>

@stop