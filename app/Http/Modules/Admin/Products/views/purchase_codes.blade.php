@extends('layouts.master')
@section('content')
    @include('Admin.Products.views.purchase_code_form')
    <table class="table table-bordered table-stripe table-responsive-md">
        <thead>
            <tr>
                {{-- header of the table --}}
                <th>Barcode</th>
                <th>Product</th>
                <th>Code</th>
                <th>Password</th>
                <th>Created_By</th>
                <th>Created_At</th>
                <th>Status</th>
                <th>Transfer to Branch</th>
            </tr>
        </thead>
        <tbody>
            {{-- check number of items in array --}}
            @if (!count($codes) > 0))
                <tr>
                    <td colspan="4">
                        <center>
                        <i>You haven't generated any codes yet.</i>
                        </center>
                    </td>
                </tr>
            @else
            @foreach ($paginate as $code)
                    <tr>
                        {{-- Barcode Coloum --}}
                        <td class="col-md-2">
                            <div class="text-center">
                                <img src={{ $code['barcode_c93'] }} draggable="false" class="img-fluid" alt="Barcode" />
                            </div>
                        </td>
                        {{-- Product Id Coloumn --}}
                        <td class="col-md-1">{{ $productsDropdown[$code['product_id']] }}</td>
                       
                        {{-- Code Coloumn --}}
                        <td class="col-md-1">{{ $code['code'] }}</td>
                        
                        {{-- Password Coloumn --}}
                        <td class="col-md-1">{{ $code['password'] }}</td>
                        
                        {{-- Owner Name Coloumn --}}
                        <td class="col-md-2">{{ $code['generated_by'] }}</td>
                        
                        {{-- Date Created Coloumn  --}}
                        <td class="col-md-2">{{ date('F j, o. - h:i A', strtotime($code['created_at'])) }} </td>
                        
                        {{-- Status Coloumn --}}
                        {{-- Status 0 = available, bootstrap = bg-primary --}}
                        {{-- Status 1 = used, bootstrap = bg-warning --}}
                        {{-- Status 2 = transfered, bootstrap = bg-danger --}}
                        <td class='col-md-2'>
                            <div class="w-100 h-75 @if($code['status'] == 0) bg-primary @elseif($code['status'] == 1) bg-danger @elseif($code['status'] == 2) bg-success @endif" >
                                @if($code['status'] == 0)
                                    Available
                                @elseif($code['status'] == 1)
                                    Bought
                                @elseif($code['status'] == 2)
                                    Transfered
                                @endif
                            </div>
                        </td>

                        {{-- Action Bar Coloumn --}}
                        {{-- Drop Down List --}}
                        {{-- and --}}
                        {{-- Submit Button --}}
                        <td class='col-md-1'>
                            {{ Form2::open(['action' => 'Admin\\Products\\Codes\\GeneratedProductCodesContoller@branch', 'method' => 'post', 'class' => 'form-inline']) }}
                               <div class='col-sm-10'>
                                    {{-- 
                                        {{ Form2::select('branch', $branches->lists('name', 'id'), ['placeholder' => 'Pick a branch.....'], ['class' => 'form-control']) }}
                                    --}}
                                    {{-- check if branch id = default id --}}
                                    {{-- if default id disabled select --}}
                                    @if($code['branch_id'] == 0)
                                        <select class='form-control' name='branch'>
                                    @else
                                        <select disabled class='form-control' name='branch'>
                                    @endif
                                        @foreach($branches as $branch)
                                            {{-- 0 id is the default id of dropdown list --}} 
                                            {{-- check if code branch id is == to default --}}
                                            
                                            @if($branch['id'] == 0)
                                                <option style='display:none' disabled selected value={{$branch['id']}}>{{$branch['name']}}</option>
                                            @else
                                                {{-- check if the code branch id is == --}}
                                                {{-- the branch id --}}
                                                {{-- then display select this option --}}
                                                @if($branch['id'] == $code['branch_id'])
                                                    <option selected value={{ $branch['id'] }}>{{ $branch['name'] }}</option>
                                                @else
                                                    {{-- add another option in the select --}}
                                                    <option value={{ $branch['id'] }}> {{ $branch['name'] }} </option>
                                                @endif
                                            @endif
                                        @endforeach
                                    </select>
                                    
                               </div>
                                {{ Form2::hidden('unique_code', $code['code'])}}
                                {{-- check if branch id is default--}}
                                {{-- display if branch id is default --}}
                                @if($code['branch_id'] == 0)
                                    {{ Form2::submit('Submit', ['class' => 'btn btn-success']) }}
                                @endif
                            {{ Form2::close() }}
                        </td>
                    </tr>
                @endforeach
                {{ $paginate->render() }}
            @endif
        </tbody>
    </table>
@stop