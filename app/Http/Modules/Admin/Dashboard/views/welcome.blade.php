@extends('layouts.master')
@section('content')
    <div class="main">
        {{-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <span class="mini-stat-icon"><i class="fa fa-key" style="font-size:48px;color:green"></i></span>
                <div class="mini-stat-info">
                    <span class="counter">{{ number_format($codes) }}</span>
                    Activation Codes
                </div>
        </div> --}}
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <span class="mini-stat-icon"><i class="fa fa-users" style="font-size:48px;color:green"></i></span>
            <div class="mini-stat-info">
                <span class="counter">{{ number_format($members) }}</span>
                Registered Members
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <span class="mini-stat-icon"><i class="fa fa-address-book " style="font-size:48px;color:green"></i></span>
                <div class="mini-stat-info">
                    <span class="counter">{{ number_format($accounts) }}</span>
                    Member Accounts
                </div>
        </div>
    </div>
    {{-- @if (!$connections->isEmpty())
        {{-- @foreach ($connections as $connect) --}}
            <?php
                // $curl = file_get_contents($connect->url . '/json-connection/' . $connect->passcode);
                // $result = json_decode($curl);
            ?>
            {{-- @if ($result != NULL)
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="panel panel-theme">
                        <div class="panel-heading">
                            <h3 class="panel-title text-center">{{ $result->company->name }}</h3>
                        </div>
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>{{ Lang::get('labels.system') }}</td>
                                    <td>{{ $result->company->app_name }}</td>
                                </tr>
                                <tr>
                                    <td>{{ Lang::get('labels.company_earnings') }}</td>
                                    <td>{{ number_format($result->earnings, 2) }}</td>
                                </tr>
                                <tr>
                                    <td>{{ Lang::get('labels.carry_left') }}</td>
                                    <td>{{ isset($result->carries->left) ? count($result->carries->left) : 0 }}</td>
                                </tr>
                                <tr>
                                    <td>{{ Lang::get('labels.carry_right') }}</td>
                                    <td>{{ isset($result->carries->right) ? count($result->carries->right) : 0 }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    @endif --}}
@stop

@section('custom_includes')

@stop
<style>
    .cen { margin: auto; max-width: 900px;}

    </style>