@extends('layouts.members')
@section('content')
    <div class="main">
        <div class="panel panel-theme rounded shadow">
            <div class="panel-heading" style="background: #0db14b;">
                <h3 class="panel-title" style="background: #0db14b;">{{ Lang::get('withdrawal.all') }}</h3>
                <div class="clearfix"></div>
            </div>
        </div>

        {{ view('widgets.withdrawals.table_widget')->with([
                    'withdrawals'=>$withdrawals,
                    'member_history' => true
                ])->render() }}

        <center>{{ $withdrawals->render() }}</center>

    </div>
@stop