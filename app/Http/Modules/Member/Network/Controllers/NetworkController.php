<?php 
namespace App\Http\Modules\Member\Network\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\AbstractHandlers\MemberAbstract;
use App\Models\User;
use App\Models\UserNetwork;
use Illuminate\Support\Facades\Auth;

class NetworkController extends MemberAbstract {

	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	protected $viewpath = 'Member.Network.views.';
	
	public function getIndex()
	{
		$head = Auth::user();
        // 1/16/2018
       
		//$head = auth()->user();
		$head['level'] = 0;
		$ids = [];
		$ids2 = [];
		$ids3 = [];
		$ids4 = [];
		$ids5 = [];
		$ids6 = [];
		$ids7 = [];
		$ids8 = [];
		$ids9 = [];
		$ids10 = [];
		$ids11 = [];
		$ids12 = [];

		$level1_user_ids = UserNetwork::select('user_id_owner')->where('user_id_sponsor', $head->id)->get();
		for($i = 0; $i < count($level1_user_ids); $i++){
			$ids[$i] = $level1_user_ids[$i]['user_id_owner'];
		}
		$level1 = User::whereIn('id', $ids)->get();

		$level2_user_ids = UserNetwork::select('user_id_owner')->whereIn('user_id_sponsor', $ids)->get();
		for($x = 0; $x < count($level2_user_ids); $x++){
			$ids2[$x] = $level2_user_ids[$x]['user_id_owner'];
		}
		$level2 = User::whereIn('id', $ids2)->get();

		$level3_user_ids = UserNetwork::select('user_id_owner')->whereIn('user_id_sponsor', $ids2)->get();
		for($w = 0; $w < count($level3_user_ids); $w++){
			$ids3[$w] = $level3_user_ids[$w]['user_id_owner'];
		}
		$level3 = User::whereIn('id', $ids3)->get();

		$level4_user_ids = UserNetwork::select('user_id_owner')->whereIn('user_id_sponsor', $ids3)->get();
		for($z = 0; $z < count($level4_user_ids); $z++){
			$ids4[$z] = $level4_user_ids[$z]['user_id_owner'];
		}
		$level4 = User::whereIn('id', $ids4)->get();

		$level5_user_ids = UserNetwork::select('user_id_owner')->whereIn('user_id_sponsor', $ids4)->get();
		for($q = 0; $q < count($level5_user_ids); $q++){
			$ids5[$q] = $level5_user_ids[$q]['user_id_owner'];
		}
		$level5 = User::whereIn('id', $ids5)->get();

		$level6_user_ids = UserNetwork::select('user_id_owner')->whereIn('user_id_sponsor', $ids5)->get();
		for($e = 0; $e < count($level6_user_ids); $e++){
			$ids6[$e] = $level6_user_ids[$e]['user_id_owner'];
		}
		$level6 = User::whereIn('id', $ids6)->get();

		$level7_user_ids = UserNetwork::select('user_id_owner')->whereIn('user_id_sponsor', $ids6)->get();
		for($x = 0; $x < count($level7_user_ids); $x++){
			$ids7[$x] = $level7_user_ids[$x]['user_id_owner'];
		}
		$level7 = User::whereIn('id', $ids7)->get();

		$level8_user_ids = UserNetwork::select('user_id_owner')->whereIn('user_id_sponsor', $ids7)->get();
		for($w = 0; $w < count($level8_user_ids); $w++){
			$ids8[$w] = $level8_user_ids[$w]['user_id_owner'];
		}
		$level8 = User::whereIn('id', $ids8)->get();

		$level9_user_ids = UserNetwork::select('user_id_owner')->whereIn('user_id_sponsor', $ids8)->get();
		for($z = 0; $z < count($level9_user_ids); $z++){
			$ids9[$z] = $level9_user_ids[$z]['user_id_owner'];
		}
		$level9 = User::whereIn('id', $ids9)->get();

		$level10_user_ids = UserNetwork::select('user_id_owner')->whereIn('user_id_sponsor', $ids9)->get();
		for($q = 0; $q < count($level10_user_ids); $q++){
			$ids10[$q] = $level10_user_ids[$q]['user_id_owner'];
		}
		$level10 = User::whereIn('id', $ids10)->get();

		$level11_user_ids = UserNetwork::select('user_id_owner')->whereIn('user_id_sponsor', $ids10)->get();
		for($e = 0; $e < count($level11_user_ids); $e++){
			$ids11[$e] = $level11_user_ids[$e]['user_id_owner'];
		}
		$level11 = User::whereIn('id', $ids11)->get();

		$level12_user_ids = UserNetwork::select('user_id_owner')->whereIn('user_id_sponsor', $ids11)->get();
		for($e = 0; $e < count($level12_user_ids); $e++){
			$ids12[$e] = $level12_user_ids[$e]['user_id_owner'];
		}
		$level12 = User::whereIn('id', $ids12)->get();

		return view($this->viewpath . 'index')->with([
			'level0' => $head,
			'level1' => $level1,
			'level2' => $level2,
			'level3' => $level3,
			'level4' => $level4,
			'level5' => $level5,
			'level6' => $level6,
			'level7' => $level7,
			'level8' => $level8,
			'level9' => $level9,
			'level10' => $level10,
			'level11' => $level11,
			'level12' => $level12
		]);
		

		
	}
	
}


