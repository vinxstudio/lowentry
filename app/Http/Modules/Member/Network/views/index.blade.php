@extends('layouts.members')
@section('content')
<?php 

$total_referral = 0;
$total_adminfee = 0; 
$total_savings = 0; 
$total_net = 0; 
?>
<div class ="main">
        {{-- <div class="container" style=>
                <img class="img" src="{{ url('img/1.jpg') }}"width="15%" height="15%;:"alt="">  
                <button class="btn">Button</button>
              </div> --}}
             
        <table class="1dataTable table table-bordered table-hover table-striped" >
                <thead>
                <th>Level</th>
                    <th>Account ID</th>
                    <th>Name</th>
                    <th>Sponsor Id</th>
                    <th>Sponsor Name</th>
                    <th>Encoded On</th>
                    <th>Referral Amount</th>
                    <th>10% Admin Fee</th>
                    <th>30% Savings</th>
                    <th>Net Earnings</th>
                    
                 
                </thead>
                <tbody style="font-size: 100%" >
            
                   
                    @foreach ($level1 as $level)
                    <?php 
                    $total_referral += 400; 
                    $total_adminfee += 40; 
                    $total_savings += 0; 
                    $total_net += 360; 
                    ?>
                    <tr>
                        <td class="hidden-xs text-center col-md-1">
                            1
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->membership_id }}
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->details->full_name}}
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->sponsor_membership_id}}
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->sponsor_name}}
                        </td>   
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->details->created_at}}
                        </td>  
                        <td class="hidden-xs text-center col-md-1">
                            400.00
                        </td>  
                        <td class="hidden-xs text-center col-md-1">
                            40.00
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                           
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                           360.00
                        </td>  
                        </tr>
    
                    @endforeach

                    @foreach ($level2 as $level)
                    <?php 
                    $total_referral += 100; 
                    $total_adminfee += 10; 
                    $total_savings += 30; 
                    $total_net += 60; 
                    ?>
                    <tr>
                        <td class="hidden-xs text-center col-md-1">
                            2
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->membership_id }} 
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->details->full_name}}
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->sponsor_membership_id}}
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->sponsor_name}}
                        </td>   
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->details->created_at}}
                        </td>  
                        <td class="hidden-xs text-center col-md-1">
                            100.00
                        </td>  
                        <td class="hidden-xs text-center col-md-1">
                            10.00
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                           30.00
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                           60.00
                        </td>  
                        </tr>
    
                    @endforeach

                    @foreach ($level3 as $level)
                    <?php 
                    $total_referral += 50; 
                    $total_adminfee += 5; 
                    $total_savings += 15; 
                    $total_net += 30; 
                    ?>
                    <tr>
                        <td class="hidden-xs text-center col-md-1">
                            3
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->membership_id }} 
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->details->full_name}}
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->sponsor_membership_id}}
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->sponsor_name}}
                        </td>    
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->details->created_at}}
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                            50.00
                        </td>  
                        <td class="hidden-xs text-center col-md-1">
                            5.00
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                           15.00
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                           30.00
                        </td>  
                        </tr>
    
                    @endforeach

                    @foreach ($level4 as $level)
                    <?php 
                    $total_referral += 30; 
                    $total_adminfee += 3; 
                    $total_savings += 9; 
                    $total_net += 18; 
                    ?>
                    <tr>
                        <td class="hidden-xs text-center col-md-1">
                            4
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->membership_id }} 
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->details->full_name}}
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->sponsor_membership_id}}
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->sponsor_name}}
                        </td>    
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->details->created_at}}
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                            30.00
                        </td>  
                        <td class="hidden-xs text-center col-md-1">
                            3.00
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                           9.00
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                           18.00
                        </td>  
                        </tr>
    
                    @endforeach

                    @foreach ($level5 as $level)
                    <?php 
                    $total_referral += 30; 
                    $total_adminfee += 3; 
                    $total_savings += 9; 
                    $total_net += 18; 
                    ?>
                    <tr>
                        <td class="hidden-xs text-center col-md-1">
                            5
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->membership_id }} 
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->details->full_name}}
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->sponsor_membership_id}}
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->sponsor_name}}
                        </td>  
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->details->created_at}}
                        </td>   
                        <td class="hidden-xs text-center col-md-1">
                            30.00
                        </td>  
                        <td class="hidden-xs text-center col-md-1">
                            3.00
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                           9.00
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                           18.00
                        </td>  
                        </tr>
    
                    @endforeach

                    @foreach ($level6 as $level)
                    <?php 
                    $total_referral += 20; 
                    $total_adminfee += 2; 
                    $total_savings += 6; 
                    $total_net += 12; 
                    ?>
                    <tr>
                        <td class="hidden-xs text-center col-md-1">
                            6
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->membership_id }} 
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->details->full_name}}
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->sponsor_membership_id}}
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->sponsor_name}}
                        </td>   
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->details->created_at}}
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                            20.00
                        </td>  
                        <td class="hidden-xs text-center col-md-1">
                            2.00
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                           6.00
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                           12.00
                        </td>  
                        </tr>
    
                    @endforeach

                    @foreach ($level7 as $level)
                    <?php 
                    $total_referral += 20; 
                    $total_adminfee += 2; 
                    $total_savings += 6; 
                    $total_net += 12; 
                    ?>
                    <tr>
                        <td class="hidden-xs text-center col-md-1">
                            7
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->membership_id }}
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->details->full_name}}
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->sponsor_membership_id}}
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->sponsor_name}}
                        </td>   
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->details->created_at}}
                        </td>  
                        <td class="hidden-xs text-center col-md-1">
                            20.00
                        </td>  
                        <td class="hidden-xs text-center col-md-1">
                            2.00
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                           6.00
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                           12.00
                        </td>  
                        </tr>
    
                    @endforeach

                    @foreach ($level8 as $level)
                    <?php 
                    $total_referral += 20; 
                    $total_adminfee += 2; 
                    $total_savings += 6; 
                    $total_net += 12; 
                    ?>
                    <tr>
                        <td class="hidden-xs text-center col-md-1">
                            8
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->membership_id }} 
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->details->full_name}}
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->sponsor_membership_id}}
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->sponsor_name}}
                        </td>   
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->details->created_at}}
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                            20.00
                        </td>  
                        <td class="hidden-xs text-center col-md-1">
                            2.00
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                           6.00
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                           12.00
                        </td>   
                        </tr>
    
                    @endforeach

                    @foreach ($level9 as $level)
                    <?php 
                    $total_referral += 20; 
                    $total_adminfee += 2; 
                    $total_savings += 6; 
                    $total_net += 12; 
                    ?>
                    <tr>
                        <td class="hidden-xs text-center col-md-1">
                            9
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->membership_id }} 
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->details->full_name}}
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->sponsor_membership_id}}
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->sponsor_name}}
                        </td>    
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->details->created_at}}
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                            20.00
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                            2.00
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                           6.00
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                           12.00
                        </td>   
                        </tr>
    
                    @endforeach

                    @foreach ($level10 as $level)
                    <?php 
                    $total_referral += 20; 
                    $total_adminfee += 2; 
                    $total_savings += 6; 
                    $total_net += 12; 
                    ?>
                    <tr>
                        <td class="hidden-xs text-center col-md-1">
                            10
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->membership_id }} 
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->details->full_name}}
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->sponsor_membership_id}}
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->sponsor_name}}
                        </td>    
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->details->created_at}}
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                            20.00
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                            2.00
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                           6.00
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                           12.00
                        </td>   
                        </tr>
    
                    @endforeach

                    @foreach ($level11 as $level)
                    <?php 
                    $total_referral += 20; 
                    $total_adminfee += 2; 
                    $total_savings += 6; 
                    $total_net += 12; 
                    ?>
                    <tr>
                        <td class="hidden-xs text-center col-md-1">
                            11
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->membership_id }} 
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->details->full_name}}
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->sponsor_membership_id}}
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->sponsor_name}}
                        </td>  
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->details->created_at}}
                        </td>   
                        <td class="hidden-xs text-center col-md-1">
                            20.00
                        </td>  
                        <td class="hidden-xs text-center col-md-1">
                            2.00
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                           6.00
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                           12.00
                        </td>  
                        </tr>
    
                    @endforeach

                    @foreach ($level12 as $level)
                    <?php 
                    $total_referral += 20; 
                    $total_adminfee += 2; 
                    $total_savings += 6; 
                    $total_net += 12; 
                    ?>
                    <tr>
                        <td class="hidden-xs text-center col-md-1">
                            12
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->membership_id }} 
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->details->full_name}}
                        </td>
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->sponsor_membership_id}}
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->sponsor_name}}
                        </td>   
                        <td class="hidden-xs text-center col-md-1">
                            {{ $level->details->created_at}}
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                            20.00
                        </td>  
                        <td class="hidden-xs text-center col-md-1">
                            2.00
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                           6.00
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                           12.00
                        </td>  
                        </tr>
    
                    @endforeach
                    <tr>
                        <td class="hidden-xs text-center col-md-1" colspan="6" style="text-align:right">
                           Total:
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                            {{number_format($total_referral,2)}}
                        </td>  
                        <td class="hidden-xs text-center col-md-1">
                        {{number_format($total_adminfee,2)}}
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                        {{number_format($total_savings,2)}}
                        </td> 
                        <td class="hidden-xs text-center col-md-1">
                        {{number_format($total_net,2)}}
                        </td>  
                    </tr>
    
        </table>
</div>
@stop