<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use App\Models\UserDetails;
use Illuminate\Http\Request;
use App\Models\UserNetwork;

class MembersMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return redirect('auth/login');
        } else {
            $auth = Auth::user();
            
            if ($auth->role != 'member') {
                return redirect('auth/login');
            }

            if (session()->has('member_pending')) {
                session()->forget('member_pending');
                session()->forget('member_pending_message');
            }
            /*
            if (!$this->check_member_required_update() && $request->route()->uri() != 'member/profile') {

                session()->put('need_profile_update', true);
                session()->put('need_profile_update_message', 'Fill up important information below to continue dashboard. <br/> Note: The red color is the important information');

                return redirect('/member/profile');
            } else {
                if ($this->check_member_required_update()) {
                    session()->forget('need_profile_update');
                    session()->forget('need_profile_update_message');
                }
            }
            */

            if ($auth->sponsor_status != UserNetwork::STATUS_ADMIN_APPROVED) {

                if ($auth->sponsor_status != UserNetwork::STATUS_SPONSOR_APPROVED && $request->route()->uri() != 'member/profile') {

                    session()->put('member_pending', true);
                    session()->put('member_pending_message', 'Sponsor still hasn\'t approved you, or you have been disapproved');

                    return redirect('/member/profile');
                }
            }

            #if (isEmailRequired() and $auth->details->email != null and $auth->verification_code != null) {
            #    return redirect('auth/verify')->with('danger', Lang::get('messages.please_verify'));
            #}
        }

        return $next($request);
    }


    function check_member_required_update()
    {

        $user = auth()->user();
        $user_details = $user->details;
        $user_address = $user->address();

        $has_required = true;

        if ($user->date_of_application == '0000-00-00 00:00:00')
        $has_required = false;
        else if (!$user_details->gender)
        $has_required = false;
        else if (!$user_details->religion)
        $has_required = false;
        else if (!$user_details->nationality)
        $has_required = false;
        else if (!$user_details->civil_status)
        $has_required = false;
        else if (!$user_details->blood_type)
        $has_required = false;
        else if (!$user_address->street)
        $has_required = false;
        else if (!$user_address->barangay)
        $has_required = false;
        else if (!$user_address->town)
        $has_required = false;
        else if (!$user_address->city)
        $has_required = false;
        else if (!$user_address->zip_code)
        $has_required = false;



        return $has_required;
    }
}
