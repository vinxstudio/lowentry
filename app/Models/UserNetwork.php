<?php 
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class UserNetwork extends Model
{
    protected $table = 'user_networks';

    const STATUS_SPONSOR_DISAPPROVED = 'Sponsor Disapproved';
    const STATUS_ADMIN_DISAPPROVED = 'Admin Disapproved';
    const STATUS_SPONSOR_APPROVED = 'Sponsor Approved';
    const STATUS_ADMIN_APPROVED = 'Admin Approved';
    const STATUS_PENDING = 'Pending';

    const STATUS = [
        self::STATUS_PENDING => 'Pending',
        self::STATUS_SPONSOR_DISAPPROVED => 'Disapproved',
        self::STATUS_ADMIN_DISAPPROVED => 'Disapproved',
        self::STATUS_SPONSOR_APPROVED => 'Approved',
        self::STATUS_ADMIN_APPROVED => 'Approved',
        null => 'Undefined'
    ];


    function request_member($sponsor_id, $status)
    {
        return $this->select(
            'user_networks.id as network_id',
            'user_networks.user_id_owner',
            'user_networks.user_id_sponsor',
            'user_networks.status',
            'user_networks.created_at',
            'user_details_owner.photo as owner_photo',
            'user_details_owner.first_name as owner_first_name',
            'user_details_owner.middle_name as owner_middle_name',
            'user_details_owner.last_name as owner_last_name',
            'user_details_sponsor.first_name as sponsor_first_name',
            'user_details_sponsor.middle_name as sponsor_middle_name',
            'user_details_sponsor.last_name as sponsor_last_name'
            
        )
            ->leftJoin('users as user_owner', 'user_owner.id', '=', 'user_networks.user_id_owner')
            ->leftJoin('users as user_sponsor', 'user_sponsor.id', '=', 'user_networks.user_id_sponsor')
            ->leftJoin('user_details as user_details_owner', 'user_details_owner.id', '=', 'user_owner.user_details_id')
            ->leftJoin('user_details as user_details_sponsor', 'user_details_sponsor.id', '=', 'user_sponsor.user_details_id')
            ->where(['user_id_sponsor' => $sponsor_id, 'status' => $status])
            ->orderBy('user_networks.created_at', 'DESC')
            ->take(30)
            ->get();
    }

    function status_type($status = null)
    {

        if ($status == self::STATUS_ADMIN_APPROVED)
        return 'Active';
        else if ($status == self::STATUS_ADMIN_DISAPPROVED)
        return 'Disabled';

        return self::STATUS[$status];
    }

    function status_actor($actor = null)
    {
        if ($actor == self::STATUS_SPONSOR_APPROVED || $actor == self::STATUS_SPONSOR_DISAPPROVED)
        return 'Sponsor';
        else if ($actor == self::STATUS_ADMIN_APPROVED || $actor == self::STATUS_ADMIN_DISAPPROVED)
        return '';
        else
        return 'Pending';
    }

    function status_admin($status = null, $type = null)
    {


        if ($type) {

            $is_approved = $type == 'APPROVED' ? true : false;
            $is_disapproved = $type == 'DISAPPROVED' ? true : false;

            if ($is_approved) {
                if ($status == self::STATUS_PENDING || $status == self::STATUS_ADMIN_APPROVED) {
                    return true;
                }
            } else if ($is_disapproved) {
                if ($status == self::STATUS_PENDING || $status == self::STATUS_ADMIN_DISAPPROVED) {
                    return true;
                }
            }
        }

        return false;
    }
}
