<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{

		//
	protected $table = 'user_addresses';


	protected $fillable = ['address_type', 'from_table_name', 'from_table_id'];

	const UserTable = 'users';

	const Provincial = 'Provincial';
	const Present = 'Present';
	const Temporary = 'Temporary';
	const Overseas = 'Overseas';

	const Address_Type = [
		self::Provincial => 'Provincial',
		self::Present => 'Present',
		self::Temporary => 'Temporary',
		self::Overseas => 'Overseas'
	];

	function single_data($table_name, $id, $address_type = self::Present)
	{

		return $this->firstOrCreate([
			'address_type' => $address_type,
			'from_table_name' => $table_name,
			'from_table_id' => $id
		]);
	}

}
