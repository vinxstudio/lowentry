<?php namespace App\Models;

class Details extends AbstractLayer
{

    protected $table = 'user_details';

    protected $appends = ['full_name'];

    function credentials()
    {
        return $this->hasOne($this->namespace . '\User', 'user_details_id', 'id');
    }

    function earnings()
    {
        return $this->hasMany($this->namespace . '\Earnings', 'user_id', 'id');
    }

    function getFullNameAttribute()
    {
        return $this->attributes['first_name'] . ' ' . $this->attributes['middle_name'] . ' ' . $this->attributes['last_name'];
    }
}
